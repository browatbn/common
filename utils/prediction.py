import os
import torch
import time
import cv2
import numpy as np
import matplotlib.pyplot as plt

from e2y.utils.nn import to_numpy, atleast4d
from e2y.vis import vis

import kornia.contrib
from torch.nn.modules.utils import _pair
import torch.nn.functional as F
import pytorch_lightning as pl


def join_tensor_patches(input, window_sizes, output_sizes, strides):

    assert len(input.shape) == 5
    batch_size, L, num_channels = input.size()[:3]

    input = input.view(batch_size, L, -1)
    input = input.permute(0, 2, 1)

    fold = torch.nn.Fold(output_sizes, window_sizes, stride=strides)
    output = fold(input)

    # average overlapping values
    input_ones = torch.ones(input.shape, dtype=input.dtype, device=input.device)
    divisor = fold(input_ones)
    return output / divisor


class MSImagePredictor:
    def __init__(
            self,
            model: pl.LightningModule,
            patch_size: int,
            target_segmentations: [dict, None] = None,
            stride: [int, None] = None,
            scales: [list, None] = None) -> None:
        self.model = model
        self.patch_size = patch_size
        if stride is None:
            stride = self.patch_size
        self.stride = stride
        self.target_segmentations = target_segmentations if target_segmentations is not None else {}
        self.scales = scales
        if self.scales is None:
            self.scales = [1]

    def _image_to_patches(self, image: torch.Tensor):
        b, c, h, w = image.shape
        npx = int(np.ceil((w - self.patch_size) / self.stride))
        npy = int(np.ceil((h - self.patch_size) / self.stride))
        w_pad = int(self.patch_size + npx * self.stride)
        h_pad = int(self.patch_size + npy * self.stride)
        padx = int(np.ceil((w_pad - w) / 2))
        pady = int(np.ceil((h_pad - h) / 2))
        print(f'padding: {padx}/{pady}')

        image_pad = torch.cat([F.pad(image[:, i:i+1], [padx, padx, pady, pady], value=-vis.IMAGENET_MEAN[i]/vis.IMAGENET_STD[i])
                           for i in range(3)],
                          dim=1)

        patches = kornia.contrib.extract_tensor_patches(image_pad,
                                                        window_size=self.patch_size,
                                                        stride=self.stride)
        self._image_size_pad = (h_pad, w_pad)
        self._image_size = (h, w)
        self._padding = (pady, padx)
        self._patch_counts = (npy+1, npx+1)
        return patches

    def _patches_to_image(self, patches: torch.Tensor):
        if len(patches.shape) == 4:
            patches = patches.unsqueeze(0)
        output_pad = join_tensor_patches(patches,
                                         window_sizes=_pair(self.patch_size),
                                         output_sizes=self._image_size_pad,
                                         strides=_pair(self.stride))
        y1, y2 = self._padding[0], self._image_size[0] + self._padding[0]
        x1, x2 = self._padding[1], self._image_size[1] + self._padding[1]
        output = output_pad[..., y1:y2, x1:x2]
        assert output.size()[-2:] == self._image_size
        return output

    def _segment_patchwise(self, image: torch.tensor):
        segmentations = {}
        if not self.target_segmentations:
            return segmentations

        image = atleast4d(image)
        patches = self._image_to_patches(image)
        print(f"Num patches: {patches.shape}")

        show = False
        if show:
            # disp_image = vis.to_disp_image(image[0])
            disp_patches = vis.to_disp_images(patches[0])
            npy, npx = self._patch_counts
            fig, ax = plt.subplots(npy, npx)
            for iy in range(npy):
                for ix in range(npx):
                    try:
                        ax[iy, ix].imshow(disp_patches[iy*npx + ix])
                    except TypeError:
                        ax.imshow(disp_patches[iy * npx + ix])
            plt.show()

            # recon_image = self._patches_to_image(patches)
            # disp_recon = vis.to_disp_image(recon_image[0])
            # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True)
            # ax[0].imshow(disp_image)
            # ax[1].imshow(disp_recon)
            # plt.show()

        assert patches.shape[0] == 1  #FIXME: handle batch size > 1
        outputs = self.model(patches.squeeze(0), as_dict=True)

        for seg_name in self.target_segmentations:
            # if hasattr(outputs, seg_name):
            if seg_name in outputs:
                seg_layer = self._patches_to_image(outputs[seg_name])
                segmentations[seg_name] = seg_layer

        return segmentations

    def _segment_image_on_scale(self, full_image: torch.tensor, scale: [int, float]):
        """
        Rescale image and predict
        :param image:
        :param scale: scale factor
        :return: recon, probs
        """
        assert torch.is_tensor(full_image)
        b, c, h, w = full_image.shape

        print(f"Scale {scale}")
        def resize_inputs(input_images):
            if scale is None or scale == 1:
                return input_images
            interpolation = 'area' if scale < 1 else 'bilinear'
            return F.interpolate(input_images, scale_factor=scale, mode=interpolation)

        image = resize_inputs(full_image)

        t = time.perf_counter()
        segmentations = self._segment_patchwise(image)
        print(f'_predict_image time: {int(1000 * (time.perf_counter() - t))}ms')

        def resize_outputs(outputs):
            if scale is not None and scale != 1:
                interpolation = 'area' if scale > 1 else 'bilinear'
                for seg_name in self.target_segmentations:
                    if seg_name in outputs:
                        outputs[seg_name] = F.interpolate(outputs[seg_name], size=(h, w), mode=interpolation)
                        # image_probs = image_probs.clamp(min=0, max=1.0)
            return outputs

        segmentations = resize_outputs(segmentations)

        # from vessels.eval_vessels import show_segmentation_results
        # show_segmentation_results(full_image[0], image[0], segmentations['vessel_mask'][0])
        # plt.show()
        return segmentations


    def segment_image(self, full_image: torch.Tensor, show=False):
        """
        Segment retinal vessel on full scale image. Prediction is performed on
        multiple image scales using overlappig crops.
        :param full_image:
        :return:
        """
        assert torch.is_tensor(full_image)
        assert len(full_image.shape) == 4

        seg_stacks = {}

        for scale in self.scales:
            segmentations = self._segment_image_on_scale(full_image, scale)
            for seg_name in segmentations:
                if seg_name in seg_stacks:
                    seg_stacks[seg_name].append(segmentations[seg_name])
                else:
                    seg_stacks[seg_name] = [segmentations[seg_name]]

        def average_segmentation_stacks(stack):
            results = {}
            for seg_name in stack:
                results[seg_name] = torch.stack(stack[seg_name]).mean(dim=0)
            return results

        merged = average_segmentation_stacks(seg_stacks)

        if show:
            fig, ax = plt.subplots(max(2, len(scales)+2), len(seg_stacks), sharex=True, sharey=True, figsize=(16,14))
            for seg_id, seg_name in enumerate(seg_stacks):
                ax[0, seg_id].set_title(seg_name, fontsize=10)
                ax[0, seg_id].imshow(vis.to_disp_image(full_image[0]))
                ax[1, seg_id].imshow(to_numpy(merged[seg_name][0,0]), cmap=plt.cm.viridis, vmin=0, vmax=1)
                for i in range(len(scales)):
                    ax[2+i, seg_id].imshow(to_numpy(seg_stacks[seg_name][i][0, 0]), cmap=plt.cm.viridis, vmin=0, vmax=1)
            plt.tight_layout()
            plt.show()

        return merged
