import time
import random
import albumentations as alb
import numpy as np
import cv2
import torch
import matplotlib.pyplot as plt
import kornia
import skimage

from csl_common import vis
from csl_common.utils.nn import to_numpy
from csl_common.utils import geometry
from csl_common.utils import imgproc
from csl_common.utils import landmark
from csl_common.utils.nn import unsqueeze


wikioptics_style = [
    alb.OneOf([
        alb.ISONoise(intensity=(1.0, 1.0), always_apply=True),
        alb.GaussNoise(var_limit=(50.0, 50.0), always_apply=True)
    ]),
    # alb.RGBShift(r_shift_limit=(100, 200), always_apply=True),
    alb.RGBShift(r_shift_limit=(100, 200), always_apply=True),
    alb.Blur(always_apply=True),
    # alb.CLAHE(always_apply=True)
]


def create_grid_points_abs(width, height, num=10, margin_x=None, margin_y=None):
    if margin_x is None:
        margin_x_rel = 0.10
        margin_x = width * margin_x_rel
    if margin_y is None:
        margin_y_rel = 0.10
        margin_y = height * margin_y_rel
    xx, yy = np.meshgrid(np.linspace(margin_x, width - margin_x, num),
                         np.linspace(margin_y, height - margin_y, num))
    return np.dstack((xx, yy)).reshape(-1, 2)  # reshape to (num, 2)


def create_grid_points_rel(num=10, margin_x_rel=0.1, margin_y_rel=0.1):
    return create_grid_points_abs(width=1.0, height=1.0, num=num, margin_x=margin_x_rel, margin_y=margin_y_rel)


def _warp_flow_fields(flow, Q):
    def map_coords(P, x, y):
        A = np.ones((6, len(x)))
        A[1:] = np.vstack([x, y, x * y, x ** 2, y ** 2])
        return P.dot(A)

    assert len(flow.shape) == 4

    if len(Q.shape) == 2:
        Q = np.array([Q] * flow.shape[0])

    assert Q.shape[0] == flow.shape[0]

    n, h, w, c = flow.shape
    new_coords = [map_coords(Q_inv, m[:, :, 0].ravel(), m[:, :, 1].ravel()) for Q_inv, m in zip(Q, flow)]
    map_xy = np.array([xy.reshape((2, h, w)).astype(np.float32) for xy in new_coords])
    return map_xy.transpose((0,2,3,1))


def quadratic_transform_to_flow(Q, output_size):
    if len(Q.shape) == 2:
        n = 1
    elif len(Q.shape) == 3:
        n = len(Q)
    else:
        raise ValueError()

    w, h = output_size[:2]
    uv_maps = init_uv_maps(n, w, h)
    return _warp_flow_fields(uv_maps, Q)


def _apply_map_to_image(img, map):
    map = torch.tensor(map)
    tensor = torch.tensor(img)
    if len(img.shape) == 2:
        tensor = tensor.unsqueeze(-1)
    if len(map.shape) == 3:
        map = map.unsqueeze(0)

    tensor = tensor.unsqueeze(0)
    tensor = tensor.permute(0, 3, 1, 2)
    warped = spatial_transform(tensor, map)
    warped = warped.permute(0, 2, 3, 1).squeeze()
    return to_numpy(warped).astype(img.dtype).reshape(img.shape)


def warp_quadratic(img: np.ndarray, Q_inv: np.ndarray):
    if img is None:
        return None
    assert len(img.shape) in [2,3]
    n, (h, w) = 1, img.shape[:2]
    flow = quadratic_transform_to_flow(Q_inv, (w,h))
    return _apply_map_to_image(img, flow)


def warp_nonrigid(img: np.ndarray, map: np.ndarray):
    if img is None:
        return None
    assert len(img.shape) in [2, 3]
    assert len(map.shape) == 3
    assert map.shape[2] == 2
    return _apply_map_to_image(img, map)


def best_fit_quadratic_transform(A, B, matches=None, max_distance=10):
    '''
    Calculates the least-squares best-fit transform that maps corresponding points A to B in m spatial dimensions
    Input:
      A: Nxm numpy array of corresponding points
      B: Nxm numpy array of corresponding points
    Returns:
      P: (2)x(6) parameters for quadratic model that maps A on to B
    '''

    if matches is not None:
        query_ids = np.array([m.queryIdx for m in matches])
        train_ids = np.array([m.trainIdx for m in matches])
        distances = np.array([m.distance for m in matches])
        good = distances < max_distance
        A = A[query_ids[good]]
        B = B[train_ids[good]]

    assert A.shape == B.shape

    def remove_nan_rows(a, b):
        assert len(a.shape) == 2
        assert len(b.shape) == 2
        non_nan = ~np.isnan(a.sum(axis=1)) & ~np.isnan(b.sum(axis=1))
        return a[non_nan], b[non_nan]

    # remove invalid points (= outside of FoV)
    A, B = remove_nan_rows(A, B)

    # get number of keypoints
    n = A.shape[0]


    if n < 6:
        raise ValueError(f"Quadratic transform needs at least 6 points (= 12 values) to compute. {n} points found.")

    R = B.T

    x = A[:, 0]
    y = A[:, 1]
    D = np.ones((6,n))
    D[1:] = np.vstack([x, y, x*y, x**2, y**2])

    DD_inv = np.linalg.inv(D.dot(D.T))
    P = R.dot(D.T.dot(DD_inv))
    return P


def quadratic_transform_points(P, coords):
    n = coords.shape[0]
    x = coords[:, 0]
    y = coords[:, 1]
    D = np.ones((6, n))
    D[1:] = np.vstack([x, y, x * y, x ** 2, y ** 2])
    new_coords = P.dot(D).T
    return new_coords.astype(np.float32)


def spatial_transform(X, map, interpolation='bilinear') -> torch.Tensor | None:
    if X is None:
        return None

    if isinstance(X, np.ndarray):
        X = torch.from_numpy(X)
    if isinstance(map, np.ndarray):
        map = torch.from_numpy(map)

    if len(X.shape) == 3:
        X = X.unsqueeze(0)
    if len(map.shape) == 3:
        map = map.unsqueeze(0)

    if X.shape[3] == 3:
        X = X.permute(0, 3, 1, 2)
    return kornia.geometry.transform.remap(X.float(), map[...,0], map[...,1], align_corners=True, mode=interpolation)


def spatial_transform_points(pts, map):

    def find_nearest_loc(array, value_x, value_y):
        assert(len(array.shape) == 3)
        l2dists = np.sqrt((array[0] - value_x)**2 + (array[1] - value_y)**2)
        idx = l2dists.argmin()
        loc = np.unravel_index(idx.item(), to_numpy(array.shape))
        min_dist = l2dists[loc[1:]]
        return loc[1], loc[2], min_dist

    def torch_find_nearest_loc(array, value_x, value_y):
        assert(len(array.shape) == 3)
        # l2dists = torch.sqrt((array[0] - value_x)**2 + (array[1] - value_y)**2)
        # idx = (torch.abs(array - value)).argmin()
        # idx = l2dists.argmin()
        # loc = np.unravel_index(idx.item(), to_numpy(array.shape))
        loc = [0,0,0]
        return loc[1:]

    if isinstance(pts, np.ndarray):
        pts = torch.from_numpy(pts)
    if isinstance(map, np.ndarray):
        map = torch.from_numpy(map)

    if len(pts.shape) == 2:
        pts = pts.unsqueeze(0)
    if len(map.shape) == 3:
        map = map.unsqueeze(0)

    b, npts, ndims = pts.shape
    b, h, w = map.shape[:3]

    xy_map = torch.tensor(init_uv_maps(b, w, h)).permute(0, 3, 1, 2).to(pts.device)

    xy_map_wrp = spatial_transform(xy_map, map, interpolation='nearest')

    pts = to_numpy(pts)
    xy_map_wrp = to_numpy(xy_map_wrp)

    new_pts = np.zeros_like(pts)
    new_pts[:] = np.nan

    for img_id in range(b):
        for pid in range(npts):
            locx, locy = pts[img_id, pid][:2]
        # if not torch.isnan(locx) and not torch.isnan(locy):
            if 0 <= locx < w and 0 <= locy < h:
                new_y, new_x, dist = find_nearest_loc(xy_map_wrp[img_id], locx, locy)
                if dist < 2.0:
                    new_pts[img_id, pid, 0] = new_x
                    new_pts[img_id, pid, 1] = new_y
            else:
                # print(locx, locy)
                pass

    # pts = pts.detach()
    # #
    # # # new_pts = pts.clone().float()
    # new_pts = torch.zeros_like(pts)
    # new_pts[:] = np.nan
    # #
    # for img_id in range(b):
    #     for pid in range(npts):
    #         locx, locy = 0,0
    #         # locx, locy = pts[img_id, pid][:2]
    #         # if not torch.isnan(locx) and not torch.isnan(locy):
    #         if 0 <= locx < w and 0 <= locy < h:
    #             new_y, new_x = torch_find_nearest_loc(xy_map_wrp[img_id], locx, locy)
    #             # new_x =0
    #             # new_y =0
    #             new_pts[img_id, pid, :] = torch.tensor([new_x, new_y])
    #         else:
    #             # print(locx, locy)
    #             pass
    return new_pts



def spatial_transform_points_affine(pts, map):
    def _apply_flow_to_points(flow, pts):
        new_pts = []
        height, width = flow.shape[:2]
        for p in pts:
            new_p = (np.nan, np.nan)
            x, y = p
            idx_x, idx_y = np.round(p).astype(int)
            if 0 <= idx_x < width and 0 <= idx_y < height:
                dx, dy = flow[idx_y, idx_x]
                # if (dx != 0 or dy != 0) and x + dx < width and y + dy < height:
                if x + dx < width and y + dy < height:
                    new_p = (x + dx, y + dy)
                else:
                    print(x,y)
            new_pts.append(new_p)
        if isinstance(pts, np.ndarray):
            new_pts = np.array(new_pts)
        return new_pts

    def remap_points(pts, map):
        h, w, c = map.shape
        uv_img = init_uv_maps(1, w, h)[0]
        flow = to_numpy(uv_img).astype(np.float32) - to_numpy(map).astype(np.float32)
        return _apply_flow_to_points(flow, to_numpy(pts))

    return remap_points(pts, map)


def init_uv_maps(_n, _w, _h):
    grid2d = np.dstack(np.meshgrid(range(_w), range(_h))).astype(np.float32)
    return grid2d[np.newaxis].repeat(_n, axis=0)


def warp_points_perspective(points_tensor, H):
    points = points_tensor

    assert torch.is_tensor(points)
    assert torch.is_tensor(H)
    assert len(points.shape) == len(H.shape) == 3
    assert points.shape[0] == H.shape[0]
    assert H.shape[-1] == H.shape[-2] == 3
    assert points.shape[-1] == 2

    b, npts, ndims = points.shape
    nan = points != points
    points[nan] = 0
    points_hom = torch.cat((points.float(), torch.ones((b, npts, 1)).cuda()), dim=2)
    points_wrp = torch.matmul(H, points_hom.transpose(1, 2)).transpose(1, 2)
    points_wrp_x = points_wrp[..., 0] / points_wrp[..., 2]
    points_wrp_y = points_wrp[..., 1] / points_wrp[..., 2]
    points_wrp = torch.stack((points_wrp_x, points_wrp_y), dim=2)
    points_wrp[nan] = np.nan
    return points_wrp
    # points_wrp[..., 1] = points_wrp[..., 1] / points_wrp[..., 2]
    # return points_wrp[..., :2]


def mean_per_image(data, mask=None):
    assert len(data.shape) >= 3
    if mask is not None:
        res = data.reshape(len(data), -1).sum(1)
        m = mask
        if len(mask.shape) == 4:
            m = m[:, 0]
        count = m.reshape(len(data), -1).sum(1)
        count = torch.clamp(count, min=1)
        res /= count
        return res
    else:
        return data.reshape(len(data), -1).mean(1)


def cmap(tensor, vmin=0, vmax=1.0, cmap=plt.cm.viridis):
    cmapped = [vis.color_map(img, vmin=vmin, vmax=vmax, cmap=cmap) for img in to_numpy(tensor)]
    return [(img * 255).astype(np.uint8) for img in cmapped]


def flow_to_hsv(flow):
    mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
    hsv = np.zeros((flow.shape[0], flow.shape[1], 3), dtype=np.uint8)
    hsv[..., 0] = ang * 180 / np.pi / 2
    # hsv[..., 1] = cv2.normalize(np.sqrt(mag), None, 0, 255, cv2.NORM_MINMAX)
    hsv[..., 1] = np.clip(np.sqrt(mag)*150, 0, 255)
    hsv[..., 2] = 255
    return cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)


def pad_homography(H, pad):
    return recenter_transform(H, pad)

def recenter_transform(M, delta):
    t = skimage.transform.AffineTransform(translation=-np.array(delta))
    A = t._inv_matrix.dot(M.dot(t.params))
    return A


def spatial_transform_numpy(img: np.ndarray, flow: np.ndarray):
    h, w = flow.shape[:2]
    flow = flow.copy()
    flow[:,:,0] += np.arange(w)
    flow[:,:,1] += np.arange(h)[:,np.newaxis]
    res = cv2.remap(img, flow, None, cv2.INTER_LINEAR)
    return res



def warp_points_flow(points, flow):
    size = flow.shape[-2]
    flow = to_numpy(flow)
    pos = np.round(points).astype(int).clip(min=0, max=size-1)
    points_new = np.zeros_like(points)
    for i in range(len(flow)):
        flow_smooth = cv2.blur(flow[i], (5,5))
        points_new[i] = points[i] + flow_smooth[pos[i,..., 1], pos[i,..., 0]]*size/2
    points_new = points_new.clip(min=0, max=size-1)
    return points_new


def make_numeric(data):
    return tuple([int(x) for x in data])


def get_affine_matrix(degrees, translation, img_size):
    import skimage.transform
    M = skimage.transform.AffineTransform(rotation=np.deg2rad(degrees), translation=translation)
    t = skimage.transform.AffineTransform(translation=-img_size/2)
    return t._inv_matrix.dot(M.params.dot(t.params))


def make_homogeneous(M):
    if M is None:
        return None
    assert M.shape[0] <= 3 and M.shape[1] <= 3
    M_hom = np.eye(3)
    M_hom[:M.shape[0], :M.shape[1]] = M
    return M_hom


def rescale_homography(H, size_homo, size_new):
    if H is None:
        return None
    H = make_homogeneous(H)
    fx = size_new[0] / size_homo[0]
    fy = size_new[1] / size_homo[1]
    M_scale_down = np.eye(3)
    M_scale_down[0, 0] = 1 / fx
    M_scale_down[1, 1] = 1 / fy
    M_scale_up = np.eye(3)
    M_scale_up[0, 0] = fx
    M_scale_up[1, 1] = fy
    return M_scale_up.dot(H.dot(M_scale_down))


def shrink_mask(m, value):
    kernel = np.ones((5, 5), np.uint8)
    return cv2.erode(m.astype(np.uint8), kernel, iterations=value)

def morph_open_mask(m, value):
    kernel = np.ones((3, 3), np.uint8)
    # return cv2.dilate(cv2.erode(m.astype(np.uint8), kernel, iterations=value), kernel, iterations=value)
    return cv2.morphologyEx(m.astype(np.uint8), cv2.MORPH_OPEN, kernel, iterations=value)


def get_bb2_from_contour(contour):
    return np.concatenate((contour.min(0), contour.max(0)))  # x1, y1, x2, y2

def get_bb1_from_contour(contour):
    return geometry.convertBB2to1(get_bb2_from_contour(contour))


# FIXME: speed up for circles
def find_nonintersecting_submask(mask, stencil, mask_on=None, min_overlap_ratio=0.10, max_iterations=200, image=None,
                                 auto_crop_stencil=True):
    def show_contours():
        # disp_img = np.zeros((mask.shape[0], mask.shape[1], 3), dtype=np.uint8)
        disp_img = image.copy()
        if mask_on is not None:
            disp_img[mask_on > 0] = (255, 255, 0)
        disp_img = cv2.drawContours(disp_img, [outer_contours], -1, (255, 0, 0), 3)             # red
        disp_img = cv2.drawContours(disp_img, [stencil_contours], -1, (0, 0, 255), 3)           # blue
        disp_img = cv2.drawContours(disp_img, [stencil_contours_shifted], -1, (0, 255, 0), 3)   # green
        disp_img = cv2.rectangle(disp_img, (min_x, min_y), (max_x, max_y), color=(255, 0, 0), thickness=3)
        plt.imshow(disp_img)
        plt.show()

    def find_stencil_shift_range(mask_contours, stencil_contours):
        mask_left, mask_top = mask_contours.min(0).astype(int)
        mask_right, mask_bottom = mask_contours.max(0).astype(int)
        stencil_left, stencil_top = stencil_contours.min(0).astype(int)
        stencil_right, stencil_bottom = stencil_contours.max(0).astype(int)

        mh, mw = mask_bottom - mask_top, mask_right - mask_left
        mcx, mcy = mask_left + mw // 2, mask_top + mh // 2
        sh, sw = stencil_bottom - stencil_top, stencil_right - stencil_left

        min_x, max_x =  mcx - max(0, mw // 2 - sw // 2), mcx + max(0, mw // 2 - sw // 2)
        min_y, max_y =  mcy - max(0, mh // 2 - sh // 2), mcy + max(0, mh // 2 - sh // 2)

        return min_x, max_x, min_y, max_y

    def constrain_stencil_center_roi_on_mask(roi, mask_contours, stencil_contours):
        mask_left, mask_top = mask_contours.min(0).astype(int)
        mask_right, mask_bottom = mask_contours.max(0).astype(int)
        stencil_left, stencil_top = stencil_contours.min(0).astype(int)
        stencil_right, stencil_bottom = stencil_contours.max(0).astype(int)

        mh, mw = mask_bottom - mask_top, mask_right - mask_left
        mcx, mcy = mask_left + mw // 2, mask_top + mh // 2
        sh, sw = stencil_bottom - stencil_top, stencil_right - stencil_left

        min_x, max_x, min_y, max_y = roi

        inner_min_x, inner_max_x = mcx - int(sw * (1 - min_overlap_ratio)), mcx + int(sw * (1 - min_overlap_ratio))
        inner_min_y, inner_max_y = mcy - int(sh * (1 - min_overlap_ratio)), mcy + int(sh * (1 - min_overlap_ratio))

        min_x, max_x = max(min_x, inner_min_x), min(max_x, inner_max_x)
        min_y, max_y = max(min_y, inner_min_y), min(max_y, inner_max_y)

        return min_x, max_x, min_y, max_y

    import time
    t = time.perf_counter()

    outer_contours = imgproc.find_longest_contour(mask)[:, 0]
    stencil_contours = imgproc.find_longest_contour(stencil)[:, 0]
    if auto_crop_stencil:
        # remove black borders
        stencil_contours -= stencil_contours.min(axis=0)

    mask_center = get_bb1_from_contour(outer_contours)[:2].astype(int)
    stencil_bbox = get_bb1_from_contour(stencil_contours).astype(int)

    val = True if mask.dtype == bool else int(mask.max())

    # find range of stencil center positions for which it would definitely be outside of mask
    # heuristic to reduce number of invalid trials
    min_x, max_x, min_y, max_y = find_stencil_shift_range(outer_contours, stencil_contours)

    needs_check_min_overlap = mask_on is not None and mask_on.sum() > 0
    if needs_check_min_overlap:
        sum_mask_on = mask_on.sum()
        # combine previous range with additional constraint that current stencil needs to overlap previous crops
        # only relevant for small crops (< 50% mask size)
        roi = min_x, max_x, min_y, max_y
        inner_contours = cv2.findContours(mask_on, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0][0][:, 0]
        min_x, max_x, min_y, max_y = constrain_stencil_center_roi_on_mask(roi, inner_contours, stencil_contours)
    # print(f"\tfindcontours: {time.perf_counter() - t}s")

    mask_area = np.sum(mask)
    trial_count = 0
    found = False
    # print(f"\t dict: {time.perf_counter() - t}s ")
    submask = mask
    pos = mask_center
    while not found and trial_count < max_iterations:
        pos = np.array((random.uniform(min_x, max_x), random.uniform(min_y, max_y)), dtype=int)
        shift = pos - stencil_bbox[:2]

        stencil_contours_shifted = stencil_contours + shift
        submask = cv2.drawContours(np.zeros_like(mask), [stencil_contours_shifted], -1, val, -1)
        union_area = np.sum(mask | submask)
        found = union_area == mask_area

        if found and needs_check_min_overlap:
            # t2 = time.perf_counter()
            overlap_area = np.sum(mask_on & submask)
            overlap_ratio = overlap_area / min(sum_mask_on, submask.sum())
            found &= overlap_ratio > min_overlap_ratio
            # if not found:
            #     show_contours()
            # print(f"\tcheck second: {time.perf_counter() - t2}s")

        # print(f"\tintersect dict: {time.perf_counter() - t}s {trial_count} {intersect}")
        trial_count += 1

    # show_contours()

    # print(f"\ttrials: {time.perf_counter() - t}s {trial_count} {stencil.shape[1]}")
    return submask, pos


def calc_controlpoint_nme(gt_lms, pred_lms):
    def reformat(lms):
        lms = to_numpy(lms).copy()
        if len(lms.shape) == 2:
            lms = lms.reshape((1,-1,2))
        return lms
    gt = reformat(gt_lms)
    pred = reformat(pred_lms)
    assert(len(gt.shape) == 3)
    assert(len(pred.shape) == 3)

    # gt[gt < 0] = np.nan
    # pred[pred < 0] = np.nan
    # norm = np.ones((len(gt), 1)) * 100.0
    # image_sizes = np.ones(gt.shape[0], dtype=np.float32) * image_size
    return np.sqrt(np.sum((gt - pred)**2, axis=2))


def contours_abs_to_rel(contours, img_shape):
    h, w = img_shape[:2]
    contours_rel = []
    for c in contours:
        c_rel = c.astype(np.float32)
        c_rel[:, 0, 0] /= w
        c_rel[:, 0, 1] /= h
        contours_rel.append(c_rel)
    return contours_rel

def contours_rel_to_abs(contours, img_shape):
    h, w = img_shape[:2]
    contours_abs = []
    for c in contours:
        c_abs = np.zeros(c.shape, dtype=np.int32)
        c_abs[:, 0, 0] = np.round(c[:, 0, 0] * w)
        c_abs[:, 0, 1] = np.round(c[:, 0, 1] * h)
        contours_abs.append(c_abs)
    return contours_abs

def points_abs_to_rel(points, img_shape):
    h, w = img_shape[:2]
    p_rel = points.astype(np.float32)
    p_rel[:, 0] /= w
    p_rel[:, 1] /= h
    return p_rel

def points_rel_to_abs(points, img_shape):
    h, w = img_shape[:2]
    p_abs = np.zeros(points.shape, dtype=np.int32)
    p_abs[:, 0] = np.round(points[:, 0] * w)
    p_abs[:, 1] = np.round(points[:, 1] * h)
    return p_abs


def mask_to_contours(mask, approx=cv2.CHAIN_APPROX_SIMPLE):
    if mask is None:
        return None
    contours = cv2.findContours(mask.astype(np.uint8), cv2.RETR_LIST, approx)[0]
    return contours_abs_to_rel(contours, mask.shape)


def contours_to_mask(contours_rel, img_shape):
    contours_abs = contours_rel_to_abs(contours_rel, img_shape)
    mask = np.zeros(img_shape[:2], dtype=np.uint8)
    cv2.drawContours(mask, contours_abs, -1, 255, -1)
    return mask


def compose_matrix(scale=None, degrees=None, translation=None, rotation_center_xy=None):
    r = skimage.transform.AffineTransform(scale=scale, rotation=np.deg2rad(degrees)).params
    R = recenter_transform(r, rotation_center_xy)
    T = skimage.transform.AffineTransform(translation=translation).params
    return R.dot(T)


# from albumentations.augmentations import functional as alb_F
from albumentations.augmentations import functional as alb_F
from albumentations.core.transforms_interface import ImageOnlyTransform

class RandomRain(ImageOnlyTransform):
    """Adds rain effects.

    From https://github.com/UjjwalSaxena/Automold--Road-Augmentation-Library

    Args:
        slant_lower: should be in range [-20, 20].
        slant_upper: should be in range [-20, 20].
        drop_length: should be in range [0, 100].
        drop_width: should be in range [1, 5].
        drop_color (list of (r, g, b)): rain lines color.
        blur_value (int): rainy view are blurry
        brightness_coefficient (float): rainy days are usually shady. Should be in range [0, 1].
        rain_type: One of [None, "drizzle", "heavy", "torrestial"]

    Targets:
        image

    Image types:
        uint8, float32
    """

    def __init__(
        self,
        slant_lower=-10,
        slant_upper=10,
        drop_length=20,
        drop_width=1,
        drop_color=(200, 200, 200),
        blur_value=7,
        brightness_coefficient=0.7,
        rain_type=None,
        always_apply=False,
        p=0.5,
    ):
        super(RandomRain, self).__init__(always_apply, p)

        if rain_type not in ["drizzle", "heavy", "torrential", None]:
            raise ValueError(
                "raint_type must be one of ({}). Got: {}".format(["drizzle", "heavy", "torrential", None], rain_type)
            )
        if not -20 <= slant_lower <= slant_upper <= 20:
            raise ValueError(
                "Invalid combination of slant_lower and slant_upper. Got: {}".format((slant_lower, slant_upper))
            )
        if not 1 <= drop_width <= 5:
            raise ValueError("drop_width must be in range [1, 5]. Got: {}".format(drop_width))
        if not 0 <= drop_length <= 100:
            raise ValueError("drop_length must be in range [0, 100]. Got: {}".format(drop_length))
        if not 0 <= brightness_coefficient <= 1:
            raise ValueError("brightness_coefficient must be in range [0, 1]. Got: {}".format(brightness_coefficient))

        self.slant_lower = slant_lower
        self.slant_upper = slant_upper

        self.drop_length = drop_length
        self.drop_width = drop_width
        self.drop_color = drop_color
        self.blur_value = blur_value
        self.brightness_coefficient = brightness_coefficient
        self.rain_type = rain_type

    def apply(self, image, slant=10, drop_length=20, rain_drops=(), **params):
        blur_kernel_size = (int(random.uniform(3, self.blur_value)) // 2) * 2 + 1
        return alb_F.add_rain(
            image,
            slant,
            drop_length,
            self.drop_width,
            self.drop_color,
            blur_kernel_size,
            self.brightness_coefficient,
            rain_drops,
        )

    @property
    def targets_as_params(self):
        return ["image"]

    def get_params_dependent_on_targets(self, params):
        img = params["image"]
        slant = int(random.uniform(self.slant_lower, self.slant_upper))

        height, width = img.shape[:2]
        area = height * width

        if self.rain_type == "drizzle":
            num_drops = area // 770
            drop_length = (5, 15)
        elif self.rain_type == "heavy":
            num_drops = width * height // 600
            drop_length = (20, 40)
        elif self.rain_type == "torrential":
            num_drops = area // 500
            drop_length = (50, 70)
        else:
            drop_length = (0, self.drop_length)
            num_drops = area // 600

        drop_length = int(random.uniform(drop_length[0], drop_length[1]))
        rain_drops = []

        for _i in range(num_drops):  # If You want heavy rain, try increasing this
            if slant < 0:
                x = random.randint(slant, width)
            else:
                x = random.randint(0, width - slant)

            y = random.randint(0, height - drop_length)

            rain_drops.append((x, y))

        return {"drop_length": drop_length, "rain_drops": rain_drops, "slant": slant}

    def get_transform_init_args_names(self):
        return (
            "slant_lower",
            "slant_upper",
            "drop_length",
            "drop_width",
            "drop_color",
            "blur_value",
            "brightness_coefficient",
            "rain_type",
        )



def create_quadratic_warps_from_landmarks(lm1, lm2):
    if len(lm1.shape) == 2:
        lm1 = unsqueeze(lm1)
    if len(lm2.shape) == 2:
        lm2 = unsqueeze(lm2)

    assert(lm1.shape == lm2.shape)
    assert(len(lm1.shape) == 3)

    batchsize = len(lm1)
    a = to_numpy(lm1)
    b = to_numpy(lm2)
    return np.array([best_fit_quadratic_transform(a[i], b[i])
                     for i in range(batchsize)])

def create_flow_from_landmarks(lms1, lms2, h, w) -> torch.Tensor:
    pad = 0
    h_pad, w_pad = h + 2*pad, w + 2*pad
    Q_inv = create_quadratic_warps_from_landmarks(lms2, lms1)
    return torch.tensor(quadratic_transform_to_flow(Q_inv, (w_pad, h_pad)))


def create_flow(batch1, batch2) -> torch.Tensor:
    n, c, h, w = batch1['image'].shape
    create_flow_from_landmarks(batch1['landmarks'], batch2['landmarks'], h, w)


def normalized_sigmoid(x, a, b):
   '''
   Returns array of a horizontal mirrored normalized sigmoid function
   output between 0 and 1
   Function parameters a = center; b = width
   '''
   s = 1/(1+np.exp(b*(x-a)))
   return cv2.normalize(s, s, 1, 0, cv2.NORM_MINMAX)


# def warp_points(points, M):
#     points = np.array(points)
#     points_hom = np.ones((points.shape[0], points.shape[1] + 1))
#     points_hom[:, :2] = points
#     new_points = M.dot(points_hom.T).T[:, :2]
#     return new_points


def warp_points(points, M):
    if points is None:
        return None
    def make_3x3(M):
        return np.vstack((M, [0, 0, 1]))
    if M.shape[0] == 2:
        M = make_3x3(M)
    return cv2.perspectiveTransform(points.reshape(-1,1,2), M).reshape(-1,2)


def create_fovea_heatmap(fovea_pos, mask_shape, sigma):
    def is_valid_coord(c):
        return not (c is None or c < 0)
    assert 0 < sigma < 1.0
    assert mask_shape is not None
    fovea_x, fovea_y = fovea_pos
    if is_valid_coord(fovea_x) and is_valid_coord(fovea_y):
        h, w = mask_shape[:2]
        sigma_px = ((h * sigma) // 2) * 2 + 1
        return landmark.create_landmark_heatmaps(
            [(fovea_x * w, fovea_y * h)],
            sigma_px,
            mask_shape
        )[0]
    else:
        return np.zeros(mask_shape[:2], dtype=np.float32)


def get_control_points(img_size, grid_size=10):
    """ Get control points in absolute pixels (float) """
    assert img_size is not None
    # create two grids of points since there are no actual control points
    pts = create_grid_points_rel(grid_size)
    # convert to pixels
    pts *= np.array(img_size[:2])
    return pts.astype(np.float32)


def crop_to_fov(sample, additional_targets=None, keypoint_params=None):
    # FIXME: use contour instead of mask image
    # contour = sample['fov_contours'][0]
    contours = imgproc.find_longest_contour(sample['fov_mask'])
    if contours is None:
        return sample
    contour = contours[:, 0]
    x_min, y_min = contour.min(axis=0)
    x_max, y_max = contour.max(axis=0)
    crop = alb.Compose(
        [alb.Crop(x_min, y_min, x_max, y_max)],
        additional_targets=additional_targets,
        keypoint_params=keypoint_params
    )
    sample = crop(**sample)
    if 'keypoints' in sample:
        sample['keypoints'] = np.array(sample['keypoints'])
    return sample


