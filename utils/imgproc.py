import cv2
import numpy as np


def find_longest_contour(mask):
    if mask.dtype == bool:
        mask = mask.astype(np.uint8) * 255
    if mask.dtype != np.uint8:
        mask = cv2.normalize(mask, None, 0, 255, cv2.NORM_MINMAX).astype(np.uint8)
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if not contours:
        return None
    id_longest_contour = np.argmax([len(x) for x in contours])
    return contours[id_longest_contour]


def is_mask(img):
    return len(img.shape) == 2 or img.shape[2] == 1


def is_grayscale(img, tol=1.0):
    if is_mask(img):
        return True
    return np.abs(img.mean() - img[..., 0].mean()) < tol  #check the avg with any element value


def create_border_contours(img: np.ndarray, threshold=10, force_convex=True, show_mask=False):
    def create_mask(img):
        # if not is_color_image(img):
        #     if img.dtype == bool:
        #         return img.astype(np.uint8)
        #     return img

        if is_mask(img):
            gray = img
        else:
            gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

        filtered = cv2.medianBlur(gray, 5)
        mask = np.array(filtered > threshold).astype(np.uint8) * 255

        # mask =  mask.astype(float) / 255
        # mask = cv2.blur(mask, (15,15))
        # kernel = np.ones((3,3), np.uint8)
        # iters = 25
        # mask = cv2.dilate(cv2.erode(mask, kernel, iterations=iters, borderType=cv2.BORDER_REPLICATE),
        #                   kernel, iterations=iters, borderType=cv2.BORDER_REPLICATE)
        # mask = cv2.erode(mask, kernel, iterations=iters)
        # mask = cv2.dilate(mask, kernel, iterations=iters)
        # plt.imshow(mask)
        # plt.show()
        return mask

    mask = create_mask(img)
    contour = find_longest_contour(mask)

    if contour is None:
        return None

    if force_convex:
        contour = cv2.convexHull(contour)

    contours = [contour]

    if show_mask:
        if img.dtype == bool:
            disp = img.astype(np.uint8).copy()
        else:
            disp = img.copy()
        mask_from_contours = cv2.drawContours(disp, contours, -1, 155, 3)
        cv2.imshow('border contours', cv2.resize(mask_from_contours, dsize=None, fx=1.0, fy=1.0))
        cv2.waitKey()

    return contours


def create_border_mask(img, threshold, show_mask=False):
    # img_ = cv2.blur(img, (25, 25))
    img_ = img
    contours = create_border_contours(img_, threshold, force_convex=True, show_mask=show_mask)
    mask = np.zeros(img_.shape[:2], dtype=np.uint8)
    if contours is None or len(contours) == 0:
        return mask
    # return cv2.fillConvexPoly(np.zeros_like(mask), contours[0], 255)
    return cv2.drawContours(np.zeros_like(mask), contours, -1, 255, -1)
