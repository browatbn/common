import numpy as np
from csl_common.utils.nn import to_numpy, to_image
from scipy import ndimage
import cv2
import torch


def gaussian(x, mean, sigma):
    return 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp(-(x-mean)**2 / (2 * sigma**2))


def make_landmark_template(wnd_size, sigma):
    X, Y = np.mgrid[-wnd_size//2:wnd_size//2, -wnd_size//2:wnd_size//2]
    Z = np.sqrt(X**2 + Y**2)
    N = gaussian(Z, 0, sigma)
    # return (N/N.max())**2  # square to make sharper
    return N / N.max()


def _fill_heatmap_layer(dst, lms, lm_id, lm_heatmap_window, wnd_size, heatmap_size):
    posx, posy = min(lms[lm_id,0], heatmap_size-1), min(lms[lm_id,1], heatmap_size-1)

    l = int(posx - wnd_size/2)
    t = int(posy - wnd_size/2)
    r = l + wnd_size
    b = t + wnd_size

    src_l = max(0, -l)
    src_t = max(0, -t)
    src_r = min(wnd_size, wnd_size-(r-heatmap_size))
    src_b = min(wnd_size, wnd_size-(b-heatmap_size))

    try:
        cn = lm_id
        wnd = lm_heatmap_window[src_t:src_b, src_l:src_r]
        weight = 1.0
        dst[cn, max(0,t):min(heatmap_size, b), max(0,l):min(heatmap_size, r)] = np.maximum(
            dst[cn, max(0,t):min(heatmap_size, b), max(0,l):min(heatmap_size, r)], wnd*weight)
    except:
        pass


def create_landmark_heatmaps(lms, sigma, heatmap_size, landmarks_to_use=None):
    lm_wnd_size = int(sigma * 5)
    lm_heatmap_window = make_landmark_template(lm_wnd_size, sigma)

    if landmarks_to_use is None:
        landmarks_to_use = range(len(lms))

    nchannels = len(landmarks_to_use)

    hms = np.zeros((nchannels, heatmap_size, heatmap_size))
    _lms = np.array(lms)
    for l in landmarks_to_use:
        wnd = lm_heatmap_window
        _fill_heatmap_layer(hms, _lms, l, wnd, lm_wnd_size, heatmap_size)

    return hms.astype(np.float32)


def to_single_channel_heatmap(lm_heatmaps):
    return to_image(lm_heatmaps.max(axis=1))


from csl_common.utils.nn import atleast3d
def get_heatmap_mode(hm):
    _m = atleast3d(hm)
    assert len(_m.shape) == 3
    y, x = ndimage.measurements.center_of_mass(to_numpy(_m.mean(0)))
    return x, y


def heatmaps_to_landmarks(hms):
    nimgs, nlms, h, w = hms.shape
    lms = np.zeros((nimgs, nlms, 2), dtype=int)
    for i in range(nimgs):
        heatmaps = to_numpy(hms[i])
        for l in range(nlms):
            hm = heatmaps[l]
            # py, px = ndimage.measurements.center_of_mass(hm)
            # lms[i, l, :] = px, py
            lms[i, l, :] = np.unravel_index(np.argmax(hm, axis=None), hm.shape)[::-1]
    return lms


def smooth_heatmaps(hms):
    assert(len(hms.shape) == 4)
    hms = to_numpy(hms)
    for i in range(hms.shape[0]):
        for l in range(hms.shape[1]):
            hms[i,l] = cv2.blur(hms[i,l], (9,9), borderType=cv2.BORDER_CONSTANT)
            # hms[i,l] = cv2.GaussianBlur(hms[i,l], (9,9), sigmaX=9, borderType=cv2.BORDER_CONSTANT)
    return hms


