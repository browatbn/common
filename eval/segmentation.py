import time

import numpy as np
from matplotlib import pyplot as plt
from skimage import filters
from sklearn.metrics import precision_recall_curve, roc_curve, auc

from e2y import vis
from e2y.utils.nn import to_numpy, atleast3d


def pixel_values_in_mask(true_vessels, pred_vessels, masks, split_by_img=False):
    if masks is None or not masks.any():
        return true_vessels, pred_vessels

    assert np.max(pred_vessels) <= 1.0 and np.min(pred_vessels) >= 0.0
    assert np.max(true_vessels) == 1.0 and np.min(true_vessels) == 0.0
    assert np.max(masks) == 1.0 and np.min(masks) == 0.0
    assert pred_vessels.shape[0] == true_vessels.shape[0] and masks.shape[0] == true_vessels.shape[0]
    assert pred_vessels.shape[1] == true_vessels.shape[1] and masks.shape[1] == true_vessels.shape[1]
    assert pred_vessels.shape[2] == true_vessels.shape[2] and masks.shape[2] == true_vessels.shape[2]

    if split_by_img:
        n = pred_vessels.shape[0]
        return (np.array([true_vessels[i, ...][masks[i, ...] == 1].flatten() for i in range(n)]),
                np.array([pred_vessels[i, ...][masks[i, ...] == 1].flatten() for i in range(n)]))
    else:
        return true_vessels[masks == 1].flatten(), pred_vessels[masks == 1].flatten()


def _f1_score(precision, recall):
    return 2 * precision * recall / (precision + recall)


def f1_score(true_vessels, pred_vessels, masks=None):
    vessels_in_mask, preds_in_mask = pixel_values_in_mask(true_vessels, pred_vessels, masks)
    t = time.time()
    precision, recall, thresholds = precision_recall_curve(
        vessels_in_mask.flatten().astype(bool), preds_in_mask.flatten(), pos_label=1)
    print(f'pr curve: {int(1000 * (time.time() - t))}ms')

    return best_f1_threshold(precision, recall, thresholds)


def best_f1_threshold(precision, recall, thresholds):
    best_f1, best_threshold = -1., None
    for index in np.linspace(0, len(precision), num=5000, endpoint=False, dtype=int):
        curr_f1 = _f1_score(precision[index], recall[index])
        if best_f1 < curr_f1:
            best_f1 = curr_f1
            best_threshold = thresholds[index]
    return best_f1, best_threshold


def misc_measures_evaluation(true_vessels, pred_vessels_bin):
    TP = np.count_nonzero(true_vessels & pred_vessels_bin)
    FN = np.count_nonzero(true_vessels & ~pred_vessels_bin)
    TN = np.count_nonzero(~true_vessels & ~pred_vessels_bin)
    FP = np.count_nonzero(~true_vessels & pred_vessels_bin)
    sensitivity = TP / (TP + FN)
    specificity = TN / (TN + FP)
    precision = TP / (TP + FP)
    acc = (TP + TN) / (TP + TN + FP + FN)
    f1 = _f1_score(precision, sensitivity)
    return acc, sensitivity, specificity, f1


def threshold_vessel_probs(probs, threshold, masks, flatten=True):
    pred_vessels_bin = np.zeros(probs.shape, dtype=np.uint8)
    pred_vessels_bin[probs >= threshold] = 1
    if flatten:
        return pred_vessels_bin[masks == 1].flatten()
    else:
        return pred_vessels_bin


def threshold_by_f1(true_vessels, generated, masks, flatten=True, f1_score=False):
    vessels_in_mask, generated_in_mask = pixel_values_in_mask(true_vessels, generated, masks)

    precision, recall, thresholds = precision_recall_curve(
        vessels_in_mask.flatten().astype(bool), generated_in_mask.flatten(), pos_label=1)
    best_f1, best_threshold = best_f1_threshold(precision, recall, thresholds)

    pred_vessels_bin = np.zeros(generated.shape)
    pred_vessels_bin[generated >= best_threshold] = 1

    if flatten:
        if f1_score:
            return pred_vessels_bin[masks == 1].flatten(), best_f1
        else:
            return pred_vessels_bin[masks == 1].flatten()
    else:
        if f1_score:
            return pred_vessels_bin, best_f1
        else:
            return pred_vessels_bin


def difference_map(ori_vessel, pred_vessel, threshold):
    assert len(ori_vessel.shape) == 2
    assert len(pred_vessel.shape) == 2
    # ori_vessel : an RGB image
    # thresholded_vessel = threshold_by_f1(np.expand_dims(ori_vessel, axis=0),
    #                                      np.expand_dims(pred_vessel, axis=0),
    #                                      np.expand_dims(mask, axis=0),
    #                                      flatten=False)

    # thresholded_vessel = np.squeeze(thresholded_vessel, axis=0)
    diff_map = np.zeros((ori_vessel.shape[0], ori_vessel.shape[1], 3))

    # Green (overlapping)
    diff_map[(ori_vessel >= threshold) & (pred_vessel >= threshold)] = (0, 255, 0)
    # Red (false negative, missing in pred)
    diff_map[(ori_vessel >= threshold) & (pred_vessel < threshold)] = (255, 0, 0)
    # Blue (false positive)
    diff_map[(ori_vessel < threshold) & (pred_vessel >= threshold)] = (0, 0, 255)

    # compute dice coefficient for a given image
    # overlap = len(diff_map[(ori_vessel == 1) & (thresholded_vessel == 1)])
    # fn = len(diff_map[(ori_vessel == 1) & (thresholded_vessel != 1)])
    # fp = len(diff_map[(ori_vessel != 1) & (thresholded_vessel == 1)])

    return diff_map


def show_segmentation_results(orig_image, recon, preds, gt_mask=None, foreground_mask=None, threshold=0.5):
    """ Show results for one image """

    disp_image = vis.to_disp_image(orig_image.squeeze(0))

    assert len(orig_image.shape) <= 3
    assert len(preds.shape) <= 3

    preds = to_numpy(preds.squeeze(0))
    pred_mask = preds > threshold

    if gt_mask is not None:
        assert len(gt_mask.shape) <= 3
        diff_map, _ = difference_map(gt_mask, preds, threshold)
        gt_mask = to_numpy(gt_mask.squeeze(0))
    else:
        diff_map = np.zeros_like(preds)
        gt_mask = np.zeros_like(preds).astype(np.uint8)

    # probs = np.clip(probs, a_min=0, a_max=1)

    fig, ax = plt.subplots(2,3, sharex=True, sharey=True)
    ax[0,0].imshow(disp_image)
    ax[0,1].imshow(gt_mask)
    # ax[0,2].imshow(errors.astype(np.uint8))
    ax[0,2].imshow(diff_map.astype(np.uint8))

    # ax[1,0].imshow(vis.to_disp_image(recon.squeeze(), denorm=True))
    # ax[1,1].imshow(preds, vmin=-1, vmax=1)
    ax[1,1].imshow(preds, vmax=1)
    ax[1,2].imshow(pred_mask.astype(np.uint8))
    plt.tight_layout()


def calculate_metrics(preds, gt_vessels, fov_masks=None, full_eval=False, verbose=False):

    assert len(preds) == len(gt_vessels)

    preds = atleast3d(to_numpy(preds))
    gt_vessels = atleast3d(to_numpy(gt_vessels))
    fov_masks = atleast3d(to_numpy(fov_masks))

    gt_vessels_in_mask, pred_vessels_in_mask = pixel_values_in_mask(gt_vessels, preds, fov_masks)

    y_true = to_numpy(gt_vessels_in_mask).ravel() >= 1
    y_score = to_numpy(pred_vessels_in_mask).ravel()

    precision, recall, thresholds = precision_recall_curve(y_true, y_score)

    precision = np.fliplr([precision])[0]  # so the array is increasing (you won't get negative AUC)
    recall = np.fliplr([recall])[0]  # so the array is increasing (you won't get negative AUC)
    thresholds = np.fliplr([thresholds])[0]
    AUC_prec_rec = np.trapz(precision, recall)
    average_precision = AUC_prec_rec

    results = {}
    results['PR'] = average_precision

    if full_eval:

        best_f1, best_f1_th = best_f1_threshold(precision, recall, thresholds)
        results['F1'] = best_f1
        results['F1_th'] = best_f1_th

        fpr, tpr, _ = roc_curve(y_true, y_score)
        roc = auc(fpr, tpr)
        results['ROC'] = roc

        otsu_threshold = filters.threshold_otsu(pred_vessels_in_mask)
        y_pred_bin = pred_vessels_in_mask >= otsu_threshold
        acc, se, sp, f1 = misc_measures_evaluation(y_true, y_pred_bin)
        results['otsu_th'] = otsu_threshold
        results['otsu_SE'] = se
        results['otsu_SP'] = sp
        results['otsu_ACC'] = acc
        results['otsu_F1'] = f1

        fixed_threshold = 0.5
        y_pred_bin = pred_vessels_in_mask >= fixed_threshold
        acc, se, sp, f1 = misc_measures_evaluation(y_true, y_pred_bin)
        results['th_SE'] = se
        results['th_SP'] = sp
        results['th_ACC'] = acc
        results['th_F1'] = f1

        if verbose:
            print(f"F1 score : {best_f1:.4f} (th={best_f1_th:.3f})")
            print(f"F1 score : {f1:.4f} (th={fixed_threshold:.3f})")
            print(f"SE/SP/ACC: {se:.4f}, {sp:.4f}, {acc:.4f} (th={fixed_threshold:.3f})")
            print('AUC PR: {0:0.4f}'.format(average_precision))
            print('AUC ROC: {0:0.4f}'.format(roc))

    return results
