import torch.utils.data as td
import albumentations as alb
import albumentations.pytorch
import numpy as np
import time
from csl_common.datasets import augment
from csl_common.utils import utils
from csl_common.utils.transforms import transform_pair, transform_sample


class _PairDataset(td.Dataset):
    _repr_indent = 4
    CONTROL_GRID_SIZE = 10

    def __init__(self,
                 dataset,
                 transform=None,
                 pairwise_transform=None,
                 rotate=0,
                 crop_scale_range=(0.7, 1.0),
                 crop_scale_sigma=0.10,
                 p_fake_pair=1.0,
                 **kwargs):

        self.dataset = dataset
        self.p_fake_pair = p_fake_pair
        self.pairwise_transfrom = pairwise_transform
        self.rotate = rotate
        self.crop_scale_range = crop_scale_range
        self.crop_scale_sigma = crop_scale_sigma
        # self.transform = transform
        # super().__init__(**kwargs)

    def __str__(self):
        str = self.dataset.__repr__()
        pad = ' ' * self._repr_indent
        str += f'\n{pad}Pairwise transform: {self.pairwise_transfrom}'
        return str

    def __len__(self):
        return len(self.dataset)


class FakePairDataset(_PairDataset):
    def __init__(self, dataset, p_fake_pair=1.0, dropout=False, crop=False, transform=None, **kwargs):
        self.dropout = dropout
        self.crop = crop
        self.additional_targets = {}
        from albumentations.pytorch import transforms as alb_torch
        self.norm = alb.Compose([alb.Normalize(), alb_torch.ToTensorV2()])
        if transform is None:
            transform = alb.Compose([], keypoint_params=alb.KeypointParams(format='xy', remove_invisible=False))
        super().__init__(dataset, p_fake_pair=p_fake_pair, transform=transform, **kwargs)

    def _create_fake_pair(self, sample1: dict, sample2: dict):
        """ turn 1 real sample into 2 fake samples (overlapping sub images) """
        if sample2 is None:
            sample2 = sample1 # no deepcopy necessary here, since apply_crop_to_sample will create a new instance
        # t = time.perf_counter()
        crop1, crop2 = augment.find_random_fov_crop_params(
            sample1,
            num_crops=2,
            scale_limit=self.crop_scale_range,
            scale_sigma=self.crop_scale_sigma,
            rotate=self.rotate,
            p=self.p_fake_pair,
            keep_aspect=True
        )
        fake_sample1 = augment.apply_crop_to_sample(sample1, crop1, self.additional_targets)
        fake_sample2 = augment.apply_crop_to_sample(sample2, crop2, self.additional_targets)
        # print(f"time fake pair: {time.perf_counter() - t}s")
        return fake_sample1, fake_sample2

    def __getitem__(self, idx):
        sample = self.dataset[idx]
        # sample1, sample2 = sample, sample

        if True:
            if 'fov_mask' not in sample:
                from csl_common.utils import imgproc
                sample['fov_mask'] = imgproc.create_border_mask(sample['image'], threshold=10)
            if isinstance(sample, dict):
                # sample = utils.crop_to_fov(sample, self.dataset.additional_targets, keypoint_params=alb.KeypointParams(format='xy', remove_invisible=False))
                # create new pair from single supplied image
                image_size = sample['image'].shape[:2][::-1]
                cp1 = utils.get_control_points(image_size, self.CONTROL_GRID_SIZE)
                sample['control_points'] = np.hstack((cp1, np.zeros_like(cp1)))
                sample1, sample2 = sample, None
            elif isinstance(sample, tuple):
                # We already received a pair of images. We still create new shifted version
                # if p_fake_pair is not zero.
                sample1, sample2 = sample
            else:
                raise ValueError("Dataset needs to return either single sample (dict) or a tuple containing"
                                 "a sample pair.")

            sample1, sample2 = self._create_fake_pair(sample1, sample2)

            if self.dropout:
                min_size = sample1['image'].shape[0] // 32
                max_size = int(1.5 * sample1['image'].shape[0] // 4)
                dropout = alb.Compose([
                    alb.CoarseDropout(min_holes=1, max_holes=3, min_height=min_size, min_width=min_size, max_height=max_size, max_width=max_size, fill_value=128, p=0.25),
                    alb.CoarseDropout(min_holes=1, max_holes=3, min_height=min_size, min_width=min_size, max_height=max_size, max_width=max_size, fill_value=0, p=0.25)]
                )
                sample1['image'] = dropout(image=sample1['image'])['image']
                sample2['image'] = dropout(image=sample2['image'])['image']

        # t = time.perf_counter()
        samples = transform_pair(sample1, sample2, transform=self.transform, norm=self.norm, crop=self.crop)
        # print(f"time transform_pair: {time.perf_counter() - t}s")

        return samples


def create(dataset_name, train=True, transform=None, num_samples=None, indices=None, repeat_factor=None,
           split=None, image_scale=1.0, make_fake_pairs=False,
           # roi='full', onh_scale=None, in_memory=False,
           **registration_params):

    assert isinstance(dataset_name, str) or isinstance(dataset_name, list)
    if indices is None:
        try:
            indices = range(num_samples)
        except TypeError:
            indices = None

    if make_fake_pairs:
        cls = FakePairDataset
    else:
        cls = FakePairDataset
        # cls = RealPairDataset

    ds = cls(sources=dataset_name, train=train, transform=transform,
             split=split, image_scale=image_scale, **registration_params)

    from csl_common.datasets.dsutil import RepeatDataset, Subset

    if repeat_factor is not None:
        ds = RepeatDataset(ds, repeat_factor)

    if indices is not None:
        indices = [i for i in indices if i < len(ds)]
        ds = Subset(ds, indices)

    return ds


class TorchvisionAdapterDataset(td.Dataset):
    additional_targets = {'fov_mask': 'mask'}

    def __init__(self, dataset, transform):
        self.dataset = dataset
        self.transform = transform
        default_transform = alb.Compose([])
        self.transform = transform if transform is not None else default_transform
        self.transform = alb.Compose([self.transform], additional_targets=self.additional_targets)
        self.norm = alb.Compose([alb.Normalize(), alb.pytorch.ToTensorV2()],
                                additional_targets=self.additional_targets)

    def __len__(self):
        return len(self.dataset)

    def _get_sample(self, idx):
        pil_image, label = self.dataset.__getitem__(idx)
        label = 1
        image = np.array(pil_image)
        if len(image.shape) == 2:
            image = np.dstack([image]*3)
        h, w = image.shape[:2]
        fov_mask = np.ones((h, w), dtype=np.uint8) * 255
        sample =  {'image': image,
                   'label': label,
                   'fov_mask': fov_mask}
        return sample


if __name__ == '__main__':
    import os
    import cv2
    import torch.utils.data as td
    import matplotlib.pyplot as plt
    import csl_common.utils.common as util
    from csl_common.vis import vis
    from csl_common.utils.nn import Batch

    util.init_random()

    ds = FakePairDataset(ap10k, p_fake_pair=1)

    print(ds)
    ds.__getitem__(0)
    batchsize = 5
    dl = td.DataLoader(ds, batch_size=batchsize, shuffle=True, num_workers=0)
    f = 1.0

    for data in dl:
        batch = Batch(data[0])
        images = vis.to_disp_images(batch.images)
        # images = vis.add_overlay_to_images(images, 1-batch.fov_masks, opacity=1.0)
        # images = vis.add_overlay_to_images(images, batch.vessel_masks, opacity=0.5)
        images = vis.add_landmarks_to_images(images, batch.control_points[...,:2], radius=2, color=(255,0,255))
        # images = vis.add_landmarks_to_images(images, batch.keypoints[:,:,:2], radius=2, color=(255,0,255))
        # if batch.targets is not None:
        #     images = vis.add_overlay_to_images(images, (batch.targets+1)/6, opacity=0.3, cmap=plt.cm.jet)
        grid1 = vis.make_grid(images, nCols=batchsize, padval=255)

        batch = Batch(data[1])
        images = vis.to_disp_images(batch.images)
        # images = vis.add_overlay_to_images(images, 1-batch.fov_masks, opacity=1.0)
        # images = vis.add_overlay_to_images(images, data[1]['vessel_mask'], opacity=0.5)
        # images = vis.add_landmarks_to_images(images, batch.control_points[:,:,:2], radius=2, color=(0,255,255))
        # images = vis.add_landmarks_to_images(images, batch.keypoints[:,:,:2], radius=2, color=(255,0,255))
        print(batch.fnames)
        grid2 = vis.make_grid(images, nCols=batchsize, padval=255)

        vis.vis_square([grid1, grid2], nCols=1, fx=f, fy=f, padval=255, title='pairs', wait=0)
