import bisect

from torch.utils import data as td


class RepeatDataset(td.Dataset):
    """
    Subset of a dataset at specified indices.

    Arguments:
        dataset (Dataset): The whole Dataset
        factor (float): Repeat the Dataset that many times
    """
    _repr_indent = 4

    def __init__(self, dataset, factor):
        factor = max(1, int(factor))
        self.dataset = dataset
        self.factor = factor

    def __getitem__(self, idx):
        return self.dataset[idx % len(self.dataset)]

    def __len__(self):
        return int(len(self.dataset) * self.factor)

    def __str__(self):
        str = self.dataset.__str__()
        pad = ' ' * self._repr_indent
        str += f'\n{pad}Repeat dataset factor: {self.factor} (=> {self.__len__()} total datapoints)'
        return str

    def _get_glaucoma_label(self, idx):
        return self.dataset._get_glaucoma_label(idx % len(self.dataset))


class Subset(td.Subset):
    _repr_indent = 4
    def __str__(self):
        str = self.dataset.__str__()
        pad = ' ' * self._repr_indent
        if len(self.indices) > 50:
            str += f'\n{pad}Subset indices: min={self.indices[0]}, max={self.indices[-1]}'
        else:
            str += f'\n{pad}Subset indices: {self.indices}'
        return str


class ConcatDataset(td.ConcatDataset):
    def __str__(self):
        return '\n'.join([ds.__str__() for ds in self.datasets])

    def _get_glaucoma_label(self, idx):
        if idx < 0:
            if -idx > len(self):
                raise ValueError("absolute value of index should not exceed dataset length")
            idx = len(self) + idx
        dataset_idx = bisect.bisect_right(self.cumulative_sizes, idx)
        if dataset_idx == 0:
            sample_idx = idx
        else:
            sample_idx = idx - self.cumulative_sizes[dataset_idx - 1]
        return self.datasets[dataset_idx]._get_glaucoma_label(sample_idx)


def concat(datasets):
    return ConcatDataset(datasets)


def upsample(ds, factor):
    assert(factor > 0)
    return RepeatDataset(ds, factor)


def downsample(ds, factor):
    if factor > 1.0:
        factor = 1.0
    if factor <= 0.0:
        raise ValueError("Dataset downsampling factor cannot be smaller than zero.")
    from sklearn.utils import shuffle
    indices = [i for i in range(len(ds))]
    indices = shuffle(indices)[:int(factor * len(indices))]
    return Subset(ds, indices)


def subset(ds, indices):
    if indices is not None:
        indices = [i for i in indices if i < len(ds)]
        ds = Subset(ds, indices)
    return ds
