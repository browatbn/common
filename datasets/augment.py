import numpy as np
import cv2
import time
import random
import torch
import matplotlib.pyplot as plt
import albumentations as alb
from csl_common.utils import utils

from csl_common.utils.nn import to_numpy
from mmpose.datasets.transforms import GenerateTarget
import config as cfg


def find_random_fov_crop_params(sample, num_crops, scale_limit=(0.6, 1.0), scale_sigma=0.05,
                               rotate=0, p=1.0, keep_aspect=False):
    image = sample['image']
    fov_mask = sample['fov_mask']

    if num_crops < 0:
        num_crops = 1

    assert len(scale_limit) == 2, "scale_limit must be tuple (low, high)"
    if scale_limit[0] > scale_limit[1]:
        scale_limit = scale_limit[::-1]

    base_scale = random.uniform(*scale_limit)

    crops = []
    cropped_area = None
    rand_value = np.random.rand()
    for i in range(num_crops):

        if rand_value > p:
            crops.append([None, None, (None, None), None])
            continue

        scale_x = random.uniform(max(scale_limit[0], base_scale - scale_sigma),
                                 min(scale_limit[1], base_scale + scale_sigma))
        if keep_aspect:
            scale_y = scale_x
            stencil = cv2.resize(fov_mask, dsize=None, fx=scale_x, fy=scale_y, interpolation=cv2.INTER_NEAREST)
        else:
            scale_y = random.uniform(max(scale_limit[0], base_scale - scale_sigma),
                                     min(scale_limit[1], base_scale + scale_sigma))
            stencil = cv2.resize(fov_mask, dsize=None, fx=scale_x, fy=scale_y, interpolation=cv2.INTER_NEAREST)
            # avoid stretching of image content if mask is not square
            keep_aspect_for_square_cnn_inputs = True
            if keep_aspect_for_square_cnn_inputs:
                def make_mask_square(m):
                    new_size = min(m.shape[:2])
                    return cv2.resize(stencil, (new_size, new_size), interpolation=cv2.INTER_NEAREST)

                stencil = make_mask_square(stencil)

        angle = random.uniform(-rotate, rotate)

        if int(angle) != 0:
            from PIL import Image
            stencil = np.array(Image.fromarray(stencil).rotate(angle, expand=1))
            # plt.imshow(stencil)
            # plt.show()

        t = time.perf_counter()
        crop_mask, center_pos = utils.find_nonintersecting_submask(fov_mask, stencil, mask_on=cropped_area,
                                                                   min_overlap_ratio=0.15, image=image)
        # print(f"time find_nonintersecting_submask: {time.perf_counter() - t}s")

        # update joint area of previous crop(s)
        if cropped_area is None:
            cropped_area = np.zeros_like(fov_mask)
        cropped_area |= crop_mask
        crops.append([crop_mask, center_pos, (scale_x, scale_y), angle])

    return crops


def apply_crop_to_sample(sample, crop, additional_targets=None):

    image = sample['image']
    fov_mask = sample['fov_mask']
    h, w, c = image.shape

    (crop_mask, crop_pos, (scale_x, scale_y), angle) = crop

    new_sample = sample.copy()
    if crop_mask is None:
        return new_sample

    # get transform from crop to original image
    center = np.array((image.shape[1] / 2, image.shape[0] / 2))
    M = utils.compose_matrix(scale=(1 / scale_x, 1 / scale_y),
                             degrees=-angle,
                             translation=center - crop_pos,
                             rotation_center_xy=center)

    # M_inv = np.linalg.inv(M)  # transformation from input image to crop

    # img = image.copy()
    # img *= crop_mask[..., np.newaxis]
    # test_img = cv2.warpAffine(img, M[:2], fov_mask.shape[:2][::-1], flags=cv2.INTER_CUBIC,
    #                           borderMode=cv2.BORDER_CONSTANT, borderValue=0)

    image_warped = cv2.warpAffine(image, M[:2], fov_mask.shape[:2][::-1], flags=cv2.INTER_LINEAR,
                                  borderMode=cv2.BORDER_CONSTANT, borderValue=0)

    # plt.imshow(image_warped)
    # img_warped *= fov_mask[..., np.newaxis]
    # for cn in range(3):
    #     image_warped[..., cn] *= fov_masks

    # select what type of fov mask should be returned
    output_fov = 'stencil'
    # output_fov = 'original_fov'

    if output_fov == 'stencil':
        new_fov_mask = fov_mask.copy()
    elif output_fov == 'original_fov':
        new_fov_mask = cv2.warpAffine(fov_mask, M[:2], fov_mask.shape[:2][::-1], flags=cv2.INTER_NEAREST,
                                      borderMode=cv2.BORDER_CONSTANT, borderValue=0)
    elif output_fov == 'circle':
        mask = np.zeros((h, w), dtype=np.uint8)
        new_fov_mask = cv2.circle(mask, (w // 2, h // 2), radius=min(h, w) // 2, color=1, thickness=-1, lineType=cv2.LINE_AA)
    elif output_fov == 'ellipse':
        mask = np.zeros((h, w), dtype=np.uint8)
        ry, rx = [int(x) for x in (h, w)]
        new_fov_mask = cv2.ellipse(mask, (w // 2, h // 2), (rx // 2, ry // 2), 0, 0, 360, 1, -1, lineType=cv2.LINE_AA)
    else:
        raise ValueError(f"Unknown FoV mask format '{output_fov}'")

    # select fov to mask warped image
    apply_fov_mask_to_output_image = False
    if apply_fov_mask_to_output_image:
        # assert fov_warped.max() < 2
        # for cn in range(3):
        #     image_warped[..., cn] *= black_border
        black_border = new_fov_mask == 0
        image_warped[black_border, :] = 0

    # plt.imshow(fov_warped)
    # plt.show()

    new_sample['image'] = image_warped
    new_sample['fov_mask'] = new_fov_mask

    # crop all masks
    if additional_targets is not None:
        for target_name, target_type in additional_targets.items():
            if (target_type == 'mask' and
                    target_name in sample and
                    sample[target_name] is not None):
                mask_warped = cv2.warpAffine(
                    sample[target_name],
                    M[:2],
                    fov_mask.shape[:2][::-1],
                    flags=cv2.INTER_NEAREST,
                    borderMode=cv2.BORDER_CONSTANT,
                    borderValue=0
                )
                mask_warped[new_fov_mask == 0] = 0
                new_sample[target_name] = mask_warped

    #
    # create aligned control points in both images
    #

    def set_points_on_background_to_nan(cps, mask):
        def in_mask(x, y):
            return mask[int(y), int(x)] > 0
        for i, cp in enumerate(cps):
            x, y = cp
            if not (0 <= x < mask.shape[1] and 0 <= y < mask.shape[0] and in_mask(x, y)):
                cps[i] = np.nan
        return cps

    def update_keypoint_visibility(cp, visibility, max_width, max_height):
        not_visible = (
                (cp[:, 0] < 0) | (cp[:, 0] >= max_width) |
                (cp[:, 1] < 0) | (cp[:, 1] >= max_height)
        )
        visibility[0, not_visible] = 0
        return visibility

    # apply homography to keypoints
    if 'control_points' in new_sample:
        cp = cv2.transform(new_sample['control_points'][:, :2][np.newaxis], M[:2]).squeeze()
        # flag invalid points
        # cp = set_points_on_background_to_nan(cp, fov_mask)
        # albumentation keypoints have two more dims (size and angle)
        new_sample['control_points'] = np.hstack((cp, np.zeros_like(cp)))

    if 'keypoints' in new_sample:
        new_sample['keypoints'] = cv2.transform(new_sample['keypoints'][:, :2][np.newaxis], M[:2]).squeeze()
        new_sample['transformed_keypoints_visible'] = update_keypoint_visibility(
            new_sample['keypoints'],
            new_sample['transformed_keypoints_visible'],
            max_width=w,
            max_height=h
        )

    # update heatmaps
    if 'heatmaps' in new_sample:
        imgsize = image.shape[:2][::-1]
        hmsize = new_sample['heatmaps'].shape[1:3][::-1]
        codec = dict(type='MSRAHeatmap', input_size=imgsize, heatmap_size=hmsize, sigma=cfg.sigma)
        new_sample['keypoints'] = new_sample['keypoints'][np.newaxis]
        new_sample['heatmaps'] = GenerateTarget(encoder=codec)(new_sample)['heatmaps']
        new_sample['keypoints'] = new_sample['keypoints'][0]

    return new_sample


def get_fov_crops(sample, crops):
    results = []
    for crop in crops:
        cropped_sample = apply_crop_to_sample(sample, crop)
        results.append(cropped_sample)
        # print(f"time warp affine: {time.perf_counter() - t_wrp}s")
        # print(f"time crop: {time.perf_counter() - t}s")
    return results



def distort_samples(X2, X2_lms, X2_cps, flow_affine, X2_fov=None):
    b, c, h, w = X2.shape

    import albumentations.augmentations.geometric.transforms
    distortion = alb.Compose([
        # alb.augmentations.geometric.transforms.OpticalDistortion(distort_limit=0.25, p=0.5,
        #                                                          border_mode=cv2.BORDER_CONSTANT,
        #                                                          value=0, mask_value=0),
        # alb.GridDistortion(distort_limit=0.25, p=0.5, normalized=True),
        alb.augmentations.geometric.transforms.OpticalDistortion(distort_limit=0.5, p=1.0,
                                                                 border_mode=cv2.BORDER_CONSTANT,
                                                                 value=0, mask_value=0),
        # alb.GridDistortion(distort_limit=0.5, p=1.0, normalized=True),
        # alb.GridDistortion(distort_limit=0.5, p=1.0, normalized=True),
        alb.RandomResizedCrop(height=h,  width=w, ratio=(1.0, 1.0))
    ], additional_targets={'uv_map': 'mask'})


    flow = torch.zeros((b, h, w, 2))
    for i, sample in enumerate(X2):
        map = utils.init_uv_maps(1, w, h)[0]
        map_wrp = distortion(image=np.zeros((h, w), dtype=np.uint8), mask=map)['mask']
        flow[i] = torch.tensor(map_wrp)

    flow = flow.cuda()

    X2_wrp = utils.spatial_transform(X2, flow)
    if X2_fov is not None:
        X2_fov_wrp = utils.spatial_transform(X2_fov, flow)
    else:
        X2_fov_wrp = None
    X2_lms_wrp = utils.spatial_transform_points(X2_lms, flow)
    X2_cps_wrp = utils.spatial_transform_points(X2_cps, flow)

    flow_total = utils.spatial_transform(flow_affine.permute(0, 3, 1, 2), flow).permute(0, 2, 3, 1)

    # img1_wrp = to_numpy(utils.spatial_transform(sample1['image'], flow_total).permute(0, 2, 3, 1))[0]/255
    # sample2_wrp['image'] = (img1_wrp * 255).astype(np.uint8)

    # fig, ax = plt.subplots(2, 3)
    # ax[0, 0].imshow(sample2['image'])
    # ax[0, 1].imshow(sample2_wrp['image'])
    # ax[0, 2].imshow(img1_wrp)
    # # ax[1, 0].imshow(utils.flow_to_hsv(flow.astype(np.float32)))
    # ax[1, 1].imshow(utils.flow_to_hsv(sample2['uv_map'].astype(np.float32)))
    # plt.show()

    return X2_wrp, X2_fov_wrp, X2_lms_wrp, X2_cps_wrp, flow_total

