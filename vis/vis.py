import cv2
from matplotlib import pyplot as plt
import numpy as np

from csl_common.utils.nn import to_numpy

IMAGENET_MEAN = (0.485, 0.456, 0.406)
IMAGENET_STD = (0.229, 0.224, 0.225)


# def denorm_tensor(tensor):
#     assert tensor.dim == 4
#     tensor[... ]

def denormalize(tensor):
    if tensor.shape[1] == 3:
        tensor[:, 0:3] *= np.array(IMAGENET_STD).reshape(1, 3, 1, 1)
        tensor[:, 0:3] += np.array(IMAGENET_MEAN).reshape(1, 3, 1, 1)
    elif tensor.shape[-1] == 3:
        tensor[..., 0:3] *= IMAGENET_STD
        tensor[..., 0:3] += IMAGENET_MEAN


def denormalized(tensor):
    if isinstance(tensor, np.ndarray):
        t = tensor.copy()
    else:
        t = tensor.clone()
    denormalize(t)
    return t


def color_map(data, vmin=None, vmax=None, cmap=plt.cm.viridis):
    if vmin is None:
        vmin = data.min()
    if vmax is None:
        vmax = data.max()
    val = np.maximum(vmin, np.minimum(vmax, data))
    norm = (val-vmin)/(vmax-vmin)
    cm = cmap(norm)
    if isinstance(cm, tuple):
        return cm[:3]
    if len(cm.shape) > 2:
        cm = cm[:,:,:3]
    return cm


# take an array of shape (n, height, width) or (n, height, width, channels)
# and visualize each (height, width) thing in a grid of size approx. sqrt(n) by sqrt(n)
def make_grid(data, padsize=2, padval=255, nCols=10, dsize=None, fx=None, fy=None, normalize=False):
    # if not isinstance(data, np.ndarray):
    data = np.array(data)
    if data.shape[0] == 0:
        return
    if data.shape[1] == 3:
        data = data.transpose((0,2,3,1))
    if data.dtype == np.float64:
        data = data.astype(np.float32)
    if normalize:
        data -= data.min()
        data /= data.max()
    else:
        data[data < 0] = 0
    #     data[data > 1] = 1

    # force the number of filters to be square
    # n = int(np.ceil(np.sqrt(data.shape[0])))
    c = nCols
    r = int(np.ceil(data.shape[0]/float(c)))

    padding = ((0, r*c - data.shape[0]), (0, padsize), (0, padsize)) + ((0, 0),) * (data.ndim - 3)
    data = np.pad(data, padding, mode='constant', constant_values=(padval, padval))

    # tile the filters into an image
    data = data.reshape((r, c) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
    data = data.reshape((r * data.shape[1], c * data.shape[3]) + data.shape[4:])

    if dsize is not None or fx is not None or fy is not None:
        # data = cv2.resize(data, dsize=dsize, fx=fx, fy=fy, interpolation=cv2.INTER_LANCZOS4)
        data = cv2.resize(data, dsize=dsize, fx=fx, fy=fy, interpolation=cv2.INTER_NEAREST)

    return data


def vis_square(data, padsize=1, padval=0, wait=0, nCols=10, title='results', dsize=None, fx=None, fy=None, normalize=False):
    img = make_grid(data, padsize=padsize, padval=padval, nCols=nCols, dsize=dsize, fx=fx, fy=fy, normalize=normalize)
    cv2.imshow(title, cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
    cv2.waitKey(wait)


def cvt32FtoU8(img):
    return (img * 255.0).astype(np.uint8)


def to_disp_image(img, denorm=True, output_dtype=np.uint8):
    if not isinstance(img, np.ndarray):
        img = img.detach().cpu().numpy()
    img = img.astype(np.float32).copy()
    if img.shape[0] == 3:
        img = img.transpose((1, 2, 0)).copy()
    if denorm and img.min() < 0:
        img = denormalized(img)
    if img.max() > 2.00:
        if isinstance(img, np.ndarray):
            img /= 255.0
        else:
            raise ValueError("Image data in wrong value range (min/max={:.2f}/{:.2f}).".format(img.min(), img.max()))
    img = np.clip(img, a_min=0, a_max=1)
    if output_dtype == np.uint8:
        img = cvt32FtoU8(img)
    if len(img.shape) == 3 and img.shape[0] == 1:
        img = img[0]
    return img


def to_disp_images(images, denorm=True, output_dtype=np.uint8):
    return [to_disp_image(i, denorm, output_dtype) for i in images]

def to_heatmap_images(heatmaps, vmin=0, vmax=1.0, cmap=plt.cm.jet):
    heatmaps = [color_map(hm, vmin=vmin, vmax=vmax, cmap=cmap) for hm in to_disp_images(heatmaps, output_dtype=np.float32)]
    return [(m * 255).astype(np.uint8) for m in heatmaps]


def get_empty_like(images):
    return [m * 0 for m in images]

def add_frames_to_images(images, labels, label_colors, gt_labels=None):
    import collections
    if not isinstance(labels, (collections.Sequence, np.ndarray)):
        labels = [labels] * len(images)
    new_images = to_disp_images(images)
    for idx, (disp, label) in enumerate(zip(new_images, labels)):
        frame_width = 3
        bgr = label_colors[label]
        cv2.rectangle(disp,
                      (frame_width // 2, frame_width // 2),
                      (disp.shape[1] - frame_width // 2, disp.shape[0] - frame_width // 2),
                      color=bgr,
                      thickness=frame_width)

        if gt_labels is not None:
            radius = 8
            color = (0, 1, 0) if gt_labels[idx] == label else (1, 0, 0)
            cv2.circle(disp, (disp.shape[1] - 2*radius, 2*radius), radius, color, -1)
    return new_images


def add_cirle_to_images(images, intensities, cmap=plt.cm.viridis, radius=10):
    new_images = to_disp_images(images)
    for idx, (disp, val) in enumerate(zip(new_images, intensities)):
        # color = (0, 1, 0) if gt_labels[idx] == label else (1, 0, 0)
        # color = plt_colors.to_rgb(val)
        if isinstance(val, float):
            color = np.array(cmap(val)) * 255
        else:
            color = val
        cv2.circle(disp, (2*radius, 2*radius), radius, color, -1, lineType=cv2.LINE_AA)
        # new_images.append(disp)
    return new_images


def get_pos_in_image(loc, text_size, image_shape):
    bottom_offset = int(6*text_size)
    right_offset = int(95*text_size)
    line_height = int(35*text_size)
    mid_offset = right_offset
    top_offset = line_height + int(0.05*line_height)
    if loc == 'tl':
        pos = (2, top_offset)
    elif loc == 'tr':
        pos = (image_shape[1]-right_offset, top_offset)
    elif loc == 'tr+1':
        pos = (image_shape[1]-right_offset, top_offset + line_height)
    elif loc == 'tr+2':
        pos = (image_shape[1]-right_offset, top_offset + line_height*2)
    elif loc == 'bl':
        pos = (2, image_shape[0]-bottom_offset)
    elif loc == 'bl-1':
        pos = (2, image_shape[0]-bottom_offset-line_height)
    elif loc == 'bl-2':
        pos = (2, image_shape[0]-bottom_offset-2*line_height)
    # elif loc == 'bm':
    #     pos = (mid_offset, image_shape[0]-bottom_offset)
    # elif loc == 'bm-1':
    #     pos = (mid_offset, image_shape[0]-bottom_offset-line_height)
    elif loc == 'br':
        pos = (image_shape[1]-right_offset, image_shape[0]-bottom_offset)
    elif loc == 'br-1':
        pos = (image_shape[1]-right_offset, image_shape[0]-bottom_offset-line_height)
    elif loc == 'br-2':
        pos = (image_shape[1]-right_offset, image_shape[0]-bottom_offset-2*line_height)
    elif loc == 'bm':
        pos = (image_shape[1]-right_offset*2, image_shape[0]-bottom_offset)
    elif loc == 'bm-1':
        pos = (image_shape[1]-right_offset*2, image_shape[0]-bottom_offset-line_height)
    elif loc == 'bm-2':
        pos = (image_shape[1]-right_offset*2, image_shape[0]-bottom_offset-2*line_height)
    else:
        raise ValueError("Unknown location {}".format(loc))
    return pos


def add_label_to_images(images, labels, gt_labels=None, loc='tl', color=(255,255,255), size=0.7, thickness=1):
    new_images = to_disp_images(images)
    _labels = to_numpy(labels)
    _gt_labels = to_numpy(gt_labels)
    for idx, (disp, val) in enumerate(zip(new_images, _labels)):
        if _gt_labels is not None:
            color = (0,255,0) if _labels[idx] == _gt_labels[idx] else (255,0,0)
        # if val != 0:
        pos = get_pos_in_image(loc, size, disp.shape)
        cv2.putText(disp, str(val), pos, cv2.FONT_HERSHEY_DUPLEX, size, color, thickness, cv2.LINE_AA)
    return new_images


def add_error_to_images(images, errors, loc='bl', size=0.65, vmin=0., vmax=30.0, thickness=1,
                        format_string='{:.2f}', cl=None, colors=None, cmap=plt.cm.jet):
    assert cl is None or colors is None
    new_images = to_disp_images(images)
    errors = to_numpy(errors)
    if cl is not None:
        colors = [cl for i in range(len(new_images))]
    if colors is None:
        colors = color_map(to_numpy(errors), cmap=cmap, vmin=vmin, vmax=vmax)
        if images[0].dtype == np.uint8:
            colors *= 255
    for disp, err, color in zip(new_images, errors, colors):
        pos = get_pos_in_image(loc, size, disp.shape)
        err_str = np.array2string(err, precision=3, separator=' ', suppress_small=True)
        # err_str = format_string.format(err)
        cv2.putText(disp, err_str, pos, cv2.FONT_HERSHEY_DUPLEX, size, color, thickness, cv2.LINE_AA)
    return new_images


# FIXME: clean up this mess!
def add_landmarks_to_images(images, landmarks, color=None, radius=2, gt_landmarks=None, keypoints_visible=None,
                            lm_errs=None, lm_confs=None, lm_rec_errs=None,
                            draw_dots=True, draw_wireframe=False, draw_gt_offsets=False, landmarks_to_draw=None,
                            offset_line_color=None, offset_line_thickness=1):

    def draw_wireframe_lines(img, lms):
        pts = lms.reshape((-1,1,2)).astype(np.int32)
        cv2.polylines(img, [pts[:17]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # head outline
        cv2.polylines(img, [pts[17:22]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # left eyebrow
        cv2.polylines(img, [pts[22:27]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # right eyebrow
        cv2.polylines(img, [pts[27:31]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # nose vert
        cv2.polylines(img, [pts[31:36]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # nose hor
        cv2.polylines(img, [pts[36:42]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # left eye
        cv2.polylines(img, [pts[42:48]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # right eye
        cv2.polylines(img, [pts[48:60]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # outer mouth
        cv2.polylines(img, [pts[60:68]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # inner mouth

    def draw_wireframe_lines_98(img, lms):
        pts = lms.reshape((-1,1,2)).astype(np.int32)
        cv2.polylines(img, [pts[:33]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # head outline
        cv2.polylines(img, [pts[33:42]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # left eyebrow
        # cv2.polylines(img, [pts[38:42]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # right eyebrow
        cv2.polylines(img, [pts[42:51]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # nose vert
        cv2.polylines(img, [pts[51:55]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # nose hor
        cv2.polylines(img, [pts[55:60]], isClosed=False, color=color, lineType=cv2.LINE_AA)  # left eye
        cv2.polylines(img, [pts[60:68]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # right eye
        cv2.polylines(img, [pts[68:76]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # outer mouth
        cv2.polylines(img, [pts[76:88]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # inner mouth
        cv2.polylines(img, [pts[88:96]], isClosed=True, color=color, lineType=cv2.LINE_AA)  # inner mouth

    def draw_offset_lines(img, lms, gt_lms, errs):
        if gt_lms.sum() == 0:
            return
        if lm_errs is None:
            # if offset_line_color is None:
            offset_line_color = (255, 255, 255)
            colors = [offset_line_color] * len(lms)
        else:
            colors = color_map(errs, cmap=plt.cm.jet, vmin=0, vmax=15.0)
        if img.dtype == np.uint8:
            colors *= 255
        for i, (p1, p2) in enumerate(zip(lms, gt_lms)):
            if landmarks_to_draw is None or i in landmarks_to_draw:
                visible = errs is None or not np.isnan(errs[i])
                valid_pair = not np.isnan(p1).any() and not np.isnan(p2).any() and p1.min() > 0 and visible
                if valid_pair:
                    cv2.line(img, tuple(p1.astype(int)), tuple(p2.astype(int)), colors[i], thickness=offset_line_thickness, lineType=cv2.LINE_AA)

    if landmarks is None:
        return images

    new_images = to_disp_images(images)
    landmarks = to_numpy(landmarks)
    gt_landmarks = to_numpy(gt_landmarks)
    lm_errs = to_numpy(lm_errs)
    img_size = new_images[0].shape[0]
    default_color = (255,255,0)

    if len(landmarks.shape) == 2:
        landmarks = landmarks[np.newaxis]

    if gt_landmarks is not None:
        if len(gt_landmarks.shape) == 2:
            gt_landmarks = gt_landmarks[np.newaxis]

        if draw_gt_offsets:
            for img_id  in range(len(new_images)):
                if gt_landmarks[img_id].sum() == 0:
                    continue
                dists = None
                if lm_errs is not None:
                    dists = lm_errs[img_id]
                draw_offset_lines(new_images[img_id], landmarks[img_id], gt_landmarks[img_id], dists)

    for img_id, (disp, lm)  in enumerate(zip(new_images, landmarks)):
        if len(lm) in [68, 21, 19, 98, 8, 5]:
            if draw_dots:
                for lm_id in range(0,len(lm)):
                    if landmarks_to_draw is None or lm_id in landmarks_to_draw or len(lm) != 68:
                        lm_color = color
                        if lm_color is None:
                            if lm_errs is not None:
                                lm_color = color_map(lm_errs[img_id, lm_id], cmap=plt.cm.jet, vmin=0, vmax=1.0)
                            else:
                                lm_color = default_color
                        # if lm_errs is not None and lm_errs[img_id, lm_id] > 40.0:
                        #     lm_color = (1,0,0)
                        cv2.circle(disp, tuple(lm[lm_id].astype(int).clip(0, disp.shape[0]-1)), radius=radius, color=lm_color, thickness=-1, lineType=cv2.LINE_AA)
                        if lm_confs is not None:
                            max_radius = img_size * 0.05
                            try:
                                conf_radius = max(2, int((1-lm_confs[img_id, lm_id]) * max_radius))
                            except ValueError:
                                conf_radius = 2
                            # if lm_confs[img_id, lm_id] > 0.4:
                            cirle_color = (0,0,255)
                            # if lm_confs[img_id, lm_id] < is_good_landmark(lm_confs, lm_rec_errs):
                            # if not is_good_landmark(lm_confs[img_id, lm_id], lm_rec_errs[img_id, lm_id]):
                            if lm_errs[img_id, lm_id] > 10.0:
                                cirle_color = (255,0,0)
                            cv2.circle(disp, tuple(lm[lm_id].astype(int)), conf_radius, cirle_color, 1, lineType=cv2.LINE_AA)

            # Draw outline if we actually have 68 valid landmarks.
            # Landmarks can be zeros for UMD landmark format (21 points).
            if draw_wireframe:
                nlms = (np.count_nonzero(lm.sum(axis=1)))
                if nlms == 68:
                    draw_wireframe_lines(disp, lm)
                elif nlms == 98:
                    draw_wireframe_lines_98(disp, lm)
        else:
            # colors = ['tab:gray', 'tab:orange', 'tab:brown', 'tab:pink', 'tab:cyan', 'tab:olive', 'tab:red', 'tab:blue']
            # colors_rgb = list(map(plt_colors.to_rgb, colors))

            # colors = sns.color_palette("Set1", n_colors=14)
            if draw_dots:
                if color is None:
                    cl = default_color
                else:
                    cl = color
                for i in range(0,len(lm)):
                    visible = keypoints_visible is None or keypoints_visible[img_id, i]
                    if not visible:
                        continue
                    x, y = lm[i].astype(int)
                    if x >=0 and y >= 0:
                        cv2.circle(disp, (x, y), radius=radius, color=cl, thickness=1, lineType=cv2.LINE_AA)
    return new_images


def draw_z(z_vecs):
    fy = 1
    width = 10
    z_zoomed = []
    for lvl, _ft in enumerate(to_numpy(z_vecs)):
        # _ft = (_ft-_ft.min())/(_ft.max()-_ft.min())
        vmin = 0 if lvl == 0 else -1

        canvas = np.zeros((int(fy*len(_ft)), width, 3))
        canvas[:int(fy*len(_ft)), :] = color_map(cv2.resize(_ft.reshape(-1,1), dsize=(width, int(fy*len(_ft))),
                                                            interpolation=cv2.INTER_NEAREST), vmin=-1.0, vmax=1.0)
        z_zoomed.append(canvas)
    return make_grid(z_zoomed, nCols=len(z_vecs), padsize=1, padval=0).transpose((1,0,2))

def prepare_overlay(img, m, cmap):
    m_new = m.copy()
    if m_new.shape != img.shape:
        m_new = cv2.resize(m_new, (img.shape[1], img.shape[0]), interpolation=cv2.INTER_CUBIC)
    m_colored = color_map(m_new**1, vmin=0, vmax=1.0, cmap=cmap)
    if len(m_colored.shape) < len(img.shape):
        m_colored = m_colored[..., np.newaxis]
    return m_colored, m_new > 0.05


def overlay_mask(img, mat, opacity=0.45, cmap=plt.cm.viridis):
    img_dtype = img.dtype
    img_new = img.copy()
    if img_new.dtype == np.uint8:
        img_new = img_new.astype(np.float32) / 255.0

    overlay, mask = prepare_overlay(img, mat, cmap)

    for c in range(3):
        img_new[..., c][mask] = img_new[..., c][mask] * (1 - opacity) + overlay[..., c][mask] * opacity

    img_new = img_new.clip(0, 1)
    if img_dtype == np.uint8:
        img_new = cvt32FtoU8(img_new)
    assert img_new.dtype == img.dtype
    assert img_new.shape == img.shape
    return img_new


def overlay_heatmap(img, mat, opacity=0.5, cmap=plt.cm.inferno):
    if img is None:
        h, w = mat.shape[-2:]
        img = np.zeros((h, w, 3), dtype=np.uint8)
    img_dtype = img.dtype
    img_new = img.copy()
    if img_new.dtype == np.uint8:
        img_new = img_new.astype(np.float32) / 255.0

    overlay, mask = prepare_overlay(img, mat, cmap)
    img_new = img_new + overlay * opacity

    img_new = img_new.clip(0, 1)
    if img_dtype == np.uint8:
        img_new = cvt32FtoU8(img_new)
    assert img_new.dtype == img.dtype
    assert img_new.shape == img.shape
    return img_new


def add_overlay_to_images(images, overlays, opacity=0.5, cmap=plt.cm.inferno):
    if overlays is None:
        return images
    assert len(images) == len(overlays)
    overlays = to_numpy(overlays)
    if len(overlays.shape) == 4 and overlays.shape[1] == 1:
        overlays = overlays[:,0]
    new_images = to_disp_images(images)
    new_images = [overlay_heatmap(disp, overlay, opacity=opacity, cmap=cmap) for disp, overlay in zip(new_images, overlays)]
    return new_images


def draw_z_vecs(levels_z, ncols, size, vmin=-1, vmax=1, vertical=False):
    z_fy = 1.0
    width, height = [int(x) for x in size[:2]]
    def draw_z(z):
        z_zoomed = []
        for lvl, ft in enumerate(z):
            _ft = ft[:]
            # _ft = (_ft-_ft.min())/(_ft.max()-_ft.min())
            # canvas = np.zeros((height, width, 3))
            if vertical:
                _ft_reshaped = _ft.reshape(-1, 1)
            else:
                _ft_reshaped = _ft.reshape(1, -1)

            canvas = color_map(
                cv2.resize(_ft_reshaped, dsize=(width, height), interpolation=cv2.INTER_NEAREST),
                vmin=vmin,
                vmax=vmax
            )

            z_zoomed.append(canvas)
        return z_zoomed

    # pivots not used anymore FIXME: remove
    def draw_pivot(z_imgs, pivot):
        z_imgs_new = to_disp_images(z_imgs)
        for new_img in z_imgs_new:
            y = int(pivot*z_fy)
            cv2.line(new_img, (0, y), (new_img.shape[1], y), (1, 1, 1), thickness=1)
        return z_imgs_new

    # pivots = [z.shape[1] for z in levels_z if z is not None]
    # z_vis_list_per_level = [draw_pivot(draw_z(z), p) for z,p in zip(levels_z, pivots) if z is not None]
    z_vis_list_per_level = [draw_z(z) for z in levels_z if z is not None]
    z_grid_per_sample = [make_grid(all_vis_sample, nCols=len(levels_z)) for all_vis_sample in zip(*z_vis_list_per_level)]
    return make_grid(z_grid_per_sample, nCols=ncols, normalize=False)


def draw_status_bar(text, status_bar_width, status_bar_height, dtype=np.float32, text_size=-1, text_color=(255,255,255)):
    img_status_bar = np.zeros((status_bar_height, status_bar_width, 3), dtype=dtype)
    if text_size <= 0:
        text_size = status_bar_height * 0.025
    cv2.putText(img_status_bar, text, (4,img_status_bar.shape[0]-5), cv2.FONT_HERSHEY_SIMPLEX, text_size, text_color, 1, cv2.LINE_AA)
    return img_status_bar
