import cv2


def draw_landmarks(img, landmarks, radius=3, wait=0):
    for lm in landmarks:
        lm_x, lm_y = lm[0], lm[1]
        cv2.circle(img, (int(lm_x), int(lm_y)), 3, (0, 0, 255), -1)
    return img


def show_landmarks(img, landmarks, radius=3, wait=0):
    disp = img.copy()
    draw_landmarks(disp, landmarks, radius=radius)
    cv2.imshow('landmarks', cv2.cvtColor(disp, cv2.COLOR_BGR2RGB))
    cv2.waitKey(wait)