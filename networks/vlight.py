import math
import collections
from typing import Type, Any, Callable, Union, List, Optional

import kornia
import torch
from torch import Tensor
from torch import nn as nn
import torch.nn.functional as F

from csl_common.utils.nn import count_parameters


def conv3x3(in_planes, out_planes, stride=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)

def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution without padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, padding=0, bias=False)


def norm2d(type):
    if type == 'batch':
        return nn.BatchNorm2d
    elif type == 'instance':
        return nn.InstanceNorm2d
    elif type == 'none':
        return nn.Identity
    else:
        raise ValueError("Invalid normalization type: ", type)


class depthwise_conv(nn.Module):
    def __init__(self, nin, nout, stride=1):
        super(depthwise_conv, self).__init__()
        self.depthwise = nn.Conv2d(nin, nin, kernel_size=3, stride=stride, padding=1, groups=nin)
        self.pointwise = nn.Conv2d(nin, nout, kernel_size=1)

    def forward(self, x):
        out = self.depthwise(x)
        out = self.pointwise(out)
        return out


class DWBasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None, layer_normalization='batch', final_relu=True):
        super().__init__()
        self.conv1 = depthwise_conv(inplanes, planes, stride=stride)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = depthwise_conv(planes, planes)
        self.layer_norm = layer_normalization
        self.bn1 = norm2d(layer_normalization)(planes)
        self.bn2 = norm2d(layer_normalization)(planes)
        self.downsample = downsample
        self.stride = stride
        self.final_relu = final_relu

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        if self.final_relu:
            out = self.relu(out)
        return out


# class BasicBlock(nn.Module):
#     expansion = 1
#
#     def __init__(
#             self,
#             inplanes: int,
#             planes: int,
#             stride: int = 1,
#             downsample: Optional[nn.Module] = None,
#             groups: int = 1,
#             base_width: int = 64,
#             dilation: int = 1,
#             norm_layer: Optional[Callable[..., nn.Module]] = None,
#     ) -> None:
#         super().__init__()
#         if norm_layer is None:
#             norm_layer = nn.BatchNorm2d
#         if groups != 1 or base_width != 64:
#             raise ValueError("BasicBlock only supports groups=1 and base_width=64")
#         if dilation > 1:
#             raise NotImplementedError("Dilation > 1 not supported in BasicBlock")
#         # Both self.conv1 and self.downsample layers downsample the input when stride != 1
#         self.conv1 = conv3x3(inplanes, planes, stride)
#         self.bn1 = norm_layer(planes)
#         self.relu = nn.ReLU(inplace=True)
#         self.conv2 = conv3x3(planes, planes)
#         self.bn2 = norm_layer(planes)
#         self.downsample = downsample
#         self.stride = stride
#
#     def forward(self, x):
#         residual = x
#
#         out = self.conv1(x)
#         out = self.bn1(out)
#         out = self.relu(out)
#
#         out = self.conv2(out)
#         out = self.bn2(out)
#
#         if self.downsample is not None:
#             residual = self.downsample(x)
#
#         out += residual
#         out = self.relu(out)
#
#         return out

class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None, layer_normalization='batch', final_relu=True):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.layer_norm = layer_normalization
        self.bn1 = norm2d(layer_normalization)(planes)
        self.bn2 = norm2d(layer_normalization)(planes)
        self.downsample = downsample
        self.stride = stride
        self.final_relu = final_relu

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        if self.final_relu:
            out = self.relu(out)
        return out


class ResnetEncoder(nn.Module):
    def __init__(self, channels, block=BasicBlock, num_blocks=2, input_channels=3, layer_normalization='batch',
                 depthwise=False):
        super().__init__()
        if depthwise:
            block = DWBasicBlock

        self.inplanes = channels[0]
        self.layer_norm = layer_normalization

        self.conv1 = nn.Conv2d(input_channels, channels[0], kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = norm2d(self.layer_norm)(channels[0])

        self.conv2 = nn.Conv2d(channels[0], channels[0], kernel_size=3, stride=2, padding=1, bias=False)
        self.bn2 = norm2d(self.layer_norm)(channels[0])

        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        # self.fc = nn.Linear(512 * block.expansion, 100)

        if len(channels) > 1:
            self.layer1 = self._make_layer(block, channels[1], num_blocks, stride=2)
        if len(channels) > 2:
            self.layer2 = self._make_layer(block, channels[2], num_blocks, stride=2)
        if len(channels) > 3:
            self.layer3 = self._make_layer(block, channels[3], num_blocks, stride=2)
        if len(channels) > 4:
            self.layer4 = self._make_layer(block, channels[4], num_blocks, stride=2)

        self.recode_layers = nn.Sequential()
        # for i in range(1):
        #     self.recode_layers.append(
        #         self._make_layer(block, channels[-1], 1, stride=1)
        #     )

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1, final_relu=True):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                norm2d(self.layer_norm)(planes * block.expansion)
            )

        layers = [block(self.inplanes, planes, stride, downsample, layer_normalization=self.layer_norm)]
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes, layer_normalization=self.layer_norm, final_relu=final_relu))
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.conv2(x)
        x = self.bn2(x)
        x = self.relu(x)

        if hasattr(self, 'layer1'):
            x = self.layer1(x)

        if hasattr(self, 'layer2'):
            x = self.layer2(x)

        if hasattr(self, 'layer3'):
           x = self.layer3(x)

        if hasattr(self, 'layer4'):
            x = self.layer4(x)

        x = self.recode_layers(x)

        if hasattr(self, 'fc'):
            x = self.avgpool(x)
            x = x.view(x.size(0), -1)
            x = self.fc(x)

        return x


class InvBasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, upsample=None, layer_normalization='batch',
                 with_spectral_norm=False, depthwise=False, upsample_mode='bilinear'):
        super(InvBasicBlock, self).__init__()
        self.layer_normalization = layer_normalization
        self.norm = norm2d(layer_normalization)
        assert upsample_mode in ['pixel_shuffle', 'bilinear', 'deconv']

        if stride != 1:
            if upsample_mode == 'pixel_shuffle':
                up = torch.nn.Sequential(
                    torch.nn.PixelShuffle(2),
                    conv1x1(inplanes // 4, planes)
                )
                # up = nn.ConvTranspose2d(self.inplanes, planes * block.expansion,
                #                         kernel_size=4, stride=stride, padding=1, bias=False)
            elif upsample_mode == 'bilinear':
                up = torch.nn.Sequential(
                    nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True),
                    conv1x1(inplanes, planes)
                )
            elif upsample_mode == 'deconv':
                # up = torch.nn.Sequential(
                #     nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True),
                #     conv1x1(inplanes, planes)
                # )
                # up = nn.ConvTranspose2d(inplanes, planes * self.expansion, kernel_size=4, stride=stride, padding=1, bias=False)
                up = nn.ConvTranspose2d(inplanes, planes * self.expansion, kernel_size=2, stride=stride, padding=0, bias=False)
            else:
                raise ValueError("Invalid upsample mode.")

            upsample = nn.Sequential(
                up,
                self.norm(planes * self.expansion),
            )
        elif inplanes != planes * self.expansion:
            upsample = nn.Sequential(
                conv3x3(self.inplanes, planes * self.expansion),
                self.norm(planes * self.expansion),
            )

        if stride != 1:
            if upsample_mode == 'pixel_shuffle':
                self.conv1 = torch.nn.Sequential(
                    torch.nn.PixelShuffle(2),
                    conv1x1(inplanes//4, planes)
                )
            elif upsample_mode == 'bilinear':
                self.conv1 = torch.nn.Sequential(
                    nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True),
                    conv3x3(inplanes, planes)
                )
            else:
                # self.conv1 = torch.nn.Sequential(
                #     nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True),
                #     conv1x1(inplanes, planes)
                # )
                # self.conv1 = nn.ConvTranspose2d(inplanes, planes, kernel_size=4, stride=stride, padding=1, bias=False)
                self.conv1 = nn.ConvTranspose2d(inplanes, planes, kernel_size=2, stride=stride, padding=0, bias=False)
        else:
            if depthwise:
                self.conv1 = depthwise_conv(inplanes, planes, stride=stride)
            else:
                self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = norm2d(layer_normalization)(planes)
        self.bn2 = norm2d(layer_normalization)(planes)
        self.relu = nn.ReLU(inplace=True)
        if depthwise:
            self.conv2 = depthwise_conv(planes, planes)
        else:
            self.conv2 = conv3x3(planes, planes)
        self.upsample = upsample
        self.stride = stride
        if with_spectral_norm:
            self.conv1 = torch.nn.utils.spectral_norm(self.conv1)
            self.conv2 = torch.nn.utils.spectral_norm(self.conv2)

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)

        if self.upsample is not None:
            residual = self.upsample(x)

        out += residual
        out = self.relu(out)

        return out


class ResnetDecoder(nn.Module):
    def __init__(self, channels, num_blocks=1, num_output_channels=3, block=None, depthwise=False,
                 extra_up=False, spectral_norm=False, layer_normalization='batch',
                 upsample_mode='deconv'):
        super().__init__()

        if block is None:
            block = InvBasicBlock

        assert upsample_mode in ['pixel_shuffle', 'bilinear', 'deconv']

        self.depthwise = depthwise
        self.extra_up = extra_up
        self.upsample_mode = upsample_mode
        self.layer_normalization = layer_normalization

        self.with_spectral_norm = spectral_norm
        if self.with_spectral_norm:
            self.sn = torch.nn.utils.spectral_norm
        else:
            self.sn = lambda x: x

        self.inplanes = channels[0]
        self.output_channels = num_output_channels

        self.norm = norm2d(layer_normalization)

        if len(channels) > 1:
            self.layer1 = self._make_layer(block, channels[1], num_blocks, stride=2, upsample_mode=upsample_mode)
        if len(channels) > 2:
            self.layer2 = self._make_layer(block, channels[2], num_blocks, stride=2, upsample_mode=upsample_mode)
        if len(channels) > 3:
            self.layer3 = self._make_layer(block, channels[3], num_blocks, stride=2, upsample_mode=upsample_mode)
        if len(channels) > 4:
            self.layer4 = self._make_layer(block, channels[4], num_blocks, stride=2, upsample_mode=upsample_mode)
        if len(channels) > 5:
            self.layer5 = self._make_layer(block, channels[5], num_blocks, stride=2, upsample_mode=upsample_mode)

        # self.fc = nn.Linear(100, channels[0])
        self.conv1 = nn.ConvTranspose2d(channels[0], channels[0], kernel_size=4, stride=1, padding=0, bias=False)
        self.bn1 = self.norm(channels[0])
        self.relu = nn.ReLU(inplace=True)


        # Skip connection scaling layers

        # Resnet style add
        # self.pw1 = conv1x1(64, 64)
        # self.pw2 = conv1x1(16, 48)
        # self.pw3 = conv1x1(16, 8)

        # Unet style concat
        # self.pw1 = conv1x1(64, 64)
        # self.pw2 = conv1x1(16+48, 48)
        # self.pw3 = conv1x1(16+8, 8)

        if self.upsample_mode == 'pixel_shuffle':
            self.up_conv = nn.Sequential(
                nn.PixelShuffle(2),
                conv1x1(channels[-1]//4, channels[-1])
            )
            self.conv_out = nn.Sequential(
                nn.PixelShuffle(2),
                conv3x3(channels[-1]//4, num_output_channels)
            )
        elif self.upsample_mode == 'bilinear':
            # self.up_conv = nn.Upsample(scale_factor=2, mode='bilinear')
            # self.up_conv = nn.Sequential(
            #     nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False),
            #     conv3x3(channels[-1], channels[-1])
            # )
            # self.conv_out = nn.Sequential(
            #     nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True),
            #     conv3x3(64, output_channels)
            # )
            self.conv_out = nn.Sequential(
                nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False),
                conv3x3(channels[-1], num_output_channels)
            )
        elif self.upsample_mode == 'deconv':
            # self.up_conv = nn.ConvTranspose2d(channels[-1], 32, kernel_size=4, stride=2, padding=1, bias=False)
            self.conv_out = nn.ConvTranspose2d(channels[-1], num_output_channels, kernel_size=4, stride=2, padding=1, bias=False)
            # self.conv_out = nn.ConvTranspose2d(channels[-1], num_output_channels, kernel_size=2, stride=2, padding=0, bias=False)
            # self.conv_out = conv3x3(channels[-1], num_output_channels)

            # self.conv_out = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False)
            # self.conv_out = nn.Sequential(
            #     nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False),
            #     conv3x3(channels[-1], num_output_channels)
            # )
        else:
            raise ValueError("Invalid upsample mode.")

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1, upsample_mode='deconv'):
        upsample = None
        layers = []
        layers.append(block(self.inplanes,
                            planes,
                            stride,
                            upsample,
                            layer_normalization=self.layer_normalization,
                            # with_spectral_norm=self.with_spectral_norm,
                            depthwise=self.depthwise,
                            upsample_mode=upsample_mode))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes,
                                planes,
                                layer_normalization=self.layer_normalization,
                                with_spectral_norm=self.with_spectral_norm,
                                depthwise=self.depthwise,
                                upsample_mode=upsample_mode))

        return nn.Sequential(*layers)

    def _forward_impl(self, x: Tensor) -> Tensor:

        if hasattr(self, 'fc'):
            x = self.fc(x)
            x = x.view(x.size(0), -1, 1,1)
            x = self.conv1(x)
            x = self.bn1(x)
            x = self.relu(x)

        if hasattr(self, 'layer1'):
            x = self.layer1(x)

        if hasattr(self, 'layer2'):
            x = self.layer2(x)

        # x = self.pw1(torch.concat([intermediates[3], x], dim=1))
        # x = x + self.pw1(intermediates[3])

        if hasattr(self, 'layer3'):
            x = self.layer3(x)

        # x = self.pw2(torch.concat([intermediates[1], x], dim=1))
        # x = x + self.pw2(intermediates[1])

        if hasattr(self, 'layer4'):
            x = self.layer4(x)

        if hasattr(self, 'layer5'):
            x = self.layer5(x)
        # x = self.pw3(torch.concat([intermediates[0], x], dim=1))
        # x = x + self.pw3(intermediates[0])
        x = self.conv_out(x)
        return x

    def forward(self, x: Tensor) -> Tensor:
        return self._forward_impl(x)


class VLight(nn.Module):
    def __init__(self, num_input_channels=3, num_output_channels=None, output_channel_names=None,
                 num_blocks=1, depthwise=False, upsample_mode='deconv', extra_up=True,
                 channels_down=None, channels_up=None, norm='batch',
                 encoder=ResnetEncoder, decoder=ResnetDecoder):
        super().__init__()

        if output_channel_names is None and num_output_channels is None:
            raise ValueError("num_out_channels and output_channel_names cannot both be None.")

        if output_channel_names is not None and num_output_channels is None:
            if num_input_channels != len(output_channel_names):
                raise ValueError("num_out_channels and output_channel_names must "
                                 "define the same number of output channels.")

        if output_channel_names is not None:
            num_output_channels = len(output_channel_names)
            self.Outputs = collections.namedtuple('outputs', output_channel_names)

        self.output_channel_names = output_channel_names

        if channels_down is None or channels_up is None:
            channels_down = [64, 128, 256, 512]
            channels_up = [512, 256, 128, 64, 32]

        self.Q = encoder(channels_down,
                         num_blocks=num_blocks,
                         input_channels=num_input_channels,
                         depthwise=depthwise,
                         layer_normalization=norm)
        # self.Q = mnet.mnet_encoder()
        # self.Q = mnet.mobilenetv3_encoder()

        self.P = ResnetDecoder(channels_up,
                               num_blocks=2,
                               num_output_channels=num_output_channels,
                               upsample_mode=upsample_mode,
                               depthwise=depthwise,
                               extra_up=extra_up,
                               layer_normalization=norm)
        # self.P = mnet.MobileNetV2Decoder()

        print("Trainable params Q: {:,}".format(count_parameters(self.Q)))
        print("Trainable params P: {:,}".format(count_parameters(self.P)))

        self.total_iter = 0
        self.iter = 0
        self.z = None

    def forward(self, X):
        z = self.Q(X)
        outputs = self.P(z)
        outputs = torch.sigmoid(outputs)

        # if self.output_channel_names is not None:
        #     d = {name: outputs[:, i].unsqueeze(1) for i, name in enumerate(self.output_channel_names)}
        #     outputs = self.Outputs(**d)

        return outputs


def load_net(model):
    from csl_common.utils import nn
    net = VLight(3,1)
    print("Loading model {}...".format(model))
    nn.read_model(model, 'saae', net)
    return net


if __name__ == '__main__':

    from pthflops import count_ops
    from torchvision.models.mobilenetv2 import mobilenet_v2
    from csl_common.networks.utils import measure_inference_latency

    device = 'cpu'

    input_size = (1, 3, 512, 512)
    # input_size = (1, 3, 256, 256)
    inp = torch.rand(input_size).to(device)

    input_size_dec = (1, 256, 32, 32)
    inp_dec = torch.rand(input_size_dec).to(device)

    # module = PreviewNet(num_output_channels=6).to(device)
    # print(f"Encoder output shape: {encoder_output_size}")

    # module = VLight(
    #     num_input_channels=3,
    #     num_output_channels=3
    # ).to(device)
    module = mnet.mnet(num_output_channels=3).to(device)
    # print(module.P)

    # module = mnet.mnet_encoder(3, quantize=False)
    # module = mnet.mobilenetv3_encoder().to(device)
    print(module)

    module.eval()

    # from quantization.vlight import vlight_decoder
    # from quantization.mnet import mnet_decoder
    # quantized_decoder = mnet_decoder(num_output_channels=3, quantize=True, backend='fbgemm')

    print(f"Decoder output shape: {module(inp).shape}")

    if hasattr(module, 'Q'):
        print("\nQ")
        count_ops(module.Q, inp, verbose=False)
        print(f'FP32 time forward CPU - Q   : {int(1000 * measure_inference_latency(module.Q, input_size))}ms')
    if hasattr(module, 'P'):
        print("\nP")
        count_ops(module.P, inp_dec, verbose=False)
        print(f'FP32 time forward CPU - P   : {int(1000 * measure_inference_latency(module.P, input_size_dec))}ms')
        # print(f'INT8 time forward CPU - P   : {int(1000 * measure_inference_latency(quantized_decoder, input_size_dec))}ms')

    print("\nP(Q)")
    count_ops(module, inp, verbose=False)
    print(f'FP32 time forward CPU - P(Q): {int(1000 * measure_inference_latency(module, input_size))}ms')

