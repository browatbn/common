from typing import Type, Any, Callable, Union, List, Optional, Sequence, Tuple

import torch.nn as nn
from torch import Tensor

from torchvision.utils import _log_api_usage_once
from torchvision.models.mobilenetv3 import InvertedResidualConfig, partial, _make_divisible
from torchvision.ops.misc import ConvNormActivation, SqueezeExcitation as SElayer


class InvertedResidual(nn.Module):
    # Implemented as described at section 5 of MobileNetV3 paper
    def __init__(
            self,
            cnf: InvertedResidualConfig,
            norm_layer: Callable[..., nn.Module],
        se_layer: Callable[..., nn.Module] = partial(SElayer, scale_activation=nn.ReLU),
    ):
        super().__init__()
        if not (1 <= cnf.stride <= 2):
            raise ValueError("illegal stride value")

        self.use_res_connect = cnf.stride == 1 and cnf.input_channels == cnf.out_channels

        layers: List[nn.Module] = []
        activation_layer = nn.Hardswish if cnf.use_hs else nn.ReLU

        # expand
        if cnf.expanded_channels != cnf.input_channels:
            layers.append(
                ConvNormActivation(
                    cnf.input_channels,
                    cnf.expanded_channels,
                    kernel_size=1,
                    norm_layer=norm_layer,
                    activation_layer=activation_layer,
                )
            )

        # depthwise
        stride = 1 if cnf.dilation > 1 else cnf.stride
        layers.append(
            ConvNormActivation(
                cnf.expanded_channels,
                cnf.expanded_channels,
                kernel_size=cnf.kernel,
                stride=stride,
                dilation=cnf.dilation,
                groups=cnf.expanded_channels,
                norm_layer=norm_layer,
                activation_layer=activation_layer,
            )
        )
        if cnf.use_se:
            squeeze_channels = _make_divisible(cnf.expanded_channels // 4, 8)
            layers.append(se_layer(cnf.expanded_channels, squeeze_channels))

        # project
        layers.append(
            ConvNormActivation(
                cnf.expanded_channels, cnf.out_channels, kernel_size=1, norm_layer=norm_layer, activation_layer=None
            )
        )

        self.block = nn.Sequential(*layers)
        self.out_channels = cnf.out_channels
        self._is_cn = cnf.stride > 1

    def forward(self, input: Tensor) -> Tensor:
        result = self.block(input)
        if self.use_res_connect:
            result += input
        return result


class MobileNetV3Encoder(nn.Module):
    def __init__(
        self,
            inverted_residual_setting: List[InvertedResidualConfig],
            block: Optional[Callable[..., nn.Module]] = None,
            norm_layer: Optional[Callable[..., nn.Module]] = None,
            num_input_channels=3,
            skip_connections=False,
            **kwargs: Any,
    ) -> None:
        """
        Encoder based on MobileNet V3

        Args:
            inverted_residual_setting (List[InvertedResidualConfig]): Network structure
            block (Optional[Callable[..., nn.Module]]): Module specifying inverted residual building block for mobilenet
            norm_layer (Optional[Callable[..., nn.Module]]): Module specifying the normalization layer to use
        """
        super().__init__()
        _log_api_usage_once(self)

        if not inverted_residual_setting:
            raise ValueError("The inverted_residual_setting should not be empty")
        elif not (
            isinstance(inverted_residual_setting, Sequence)
            and all([isinstance(s, InvertedResidualConfig) for s in inverted_residual_setting])
        ):
            raise TypeError("The inverted_residual_setting should be List[InvertedResidualConfig]")

        if block is None:
            block = InvertedResidual

        self.block_type = block
        self.skip_connections = skip_connections

        if norm_layer is None:
            norm_layer = partial(nn.BatchNorm2d, eps=0.001, momentum=0.01)

        layers: List[nn.Module] = []

        # building first layer
        firstconv_output_channels = inverted_residual_setting[0].input_channels
        layers.append(
            ConvNormActivation(
                num_input_channels,
                firstconv_output_channels,
                kernel_size=3,
                stride=2,
                norm_layer=norm_layer,
                # activation_layer=nn.Hardswish,
                activation_layer=nn.ReLU,
            )
        )

        # building inverted residual blocks
        for cnf in inverted_residual_setting:
            layers.append(block(cnf, norm_layer))

        self.features = nn.Sequential(*layers)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode="fan_out")
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.zeros_(m.bias)

    # Todo: add type annotation for skip connection output
    def _forward_impl(self, x: Tensor) -> Tensor:
        # intermediates = []
        # if self.skip_connections:
        #     for ft in self.features:
        #         if type(ft) is self.block_type:
        #             intermediates.append(x)
        #         x = ft(x)
        # else:
        #     x = self.features(x)
        x = self.features(x)
        # disable skip connections for now
        return x #, intermediates

    def forward(self, x: Tensor) -> Tensor:
        return self._forward_impl(x)


def _mobilenet_v3_encoder_conf(
        arch: str, width_mult: float = 1.0, reduced_tail: bool = False, dilated: bool = False, **kwargs: Any
):
    reduce_divider = 2 if reduced_tail else 1

    bneck_conf = partial(InvertedResidualConfig, width_mult=width_mult)
    adjust_channels = partial(InvertedResidualConfig.adjust_channels, width_mult=width_mult)

    d = 2
    t = 2

    if arch == "large":
        inverted_residual_setting = [
            bneck_conf(16, 3, 16, 16, False, "RE", 2, 1),  # 256 -> 128

            bneck_conf(16, 3, 64, 24 * d, False, "RE", 2, 1),  # C1   # 128 -> 64
            bneck_conf(24 * d, 3, 72 * t, 24 * d, False, "RE", 1, 1),

            bneck_conf(24 * d, 5, 72 * t, 40 * d, True, "RE", 2, 1),  # C2  # 64 -> 32
            bneck_conf(40 * d, 5, 120 * t, 40 * d, True, "RE", 1, 1),
            bneck_conf(40 * d, 5, 120 * t, 40 * d, True, "RE", 1, 1),

            bneck_conf(40 * d, 3, 112 * t, 256, False, "RE", 1, 1),
        ]
        last_channel = adjust_channels(1280 // reduce_divider)  # C5
    elif arch == "large_skip":
        inverted_residual_setting = [
            bneck_conf(16, 3, 16, 16, False, "RE", 2, 1),  # 256 -> 128

            bneck_conf(16, 3, 64, 32 * d, False, "RE", 2, 1),  # C1   # 128 -> 64
            bneck_conf(32 * d, 3, 72 * t, 32 * d, False, "RE", 1, 1),

            bneck_conf(32 * d, 5, 72 * t, 40 * d, True, "RE", 2, 1),  # C2  # 64 -> 32
            bneck_conf(40 * d, 5, 120 * t, 40 * d, True, "RE", 1, 1),
            bneck_conf(40 * d, 5, 120 * t, 40 * d, True, "RE", 1, 1),

            bneck_conf(40 * d, 3, 112 * t, 256, False, "RE", 1, 1),
        ]
        last_channel = adjust_channels(1280 // reduce_divider)  # C5

    elif arch == "small":
        inverted_residual_setting = [
            bneck_conf(16, 3, 16, 16, True, "RE", 2, 1),  # C1
            bneck_conf(16, 3, 72, 24, False, "RE", 2, 1),  # C2
            bneck_conf(24, 3, 88, 24, False, "RE", 1, 1),
            bneck_conf(24, 5, 96, 40, True, "HS", 2, 1),  # C3
            bneck_conf(40, 5, 240, 40, True, "HS", 1, 1),
            bneck_conf(40, 5, 240, 40, True, "HS", 1, 1),
            bneck_conf(40, 5, 120, 48, True, "HS", 1, 1),
            bneck_conf(48, 5, 144, 48, True, "HS", 1, 1),

            bneck_conf(46, 3, 112, 256, False, "HS", 1, 1),
        ]
        last_channel = adjust_channels(1024 // reduce_divider)  # C
    else:
        raise ValueError(f"Unsupported model type {arch}")

    return inverted_residual_setting, last_channel
