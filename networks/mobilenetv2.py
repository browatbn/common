from typing import Type, Any, Callable, Union, List, Optional

import torch
import torch.nn as nn
from torch import Tensor

from torchvision.models._utils import _make_divisible
from torchvision.ops.misc import ConvNormActivation
from torchvision.utils import _log_api_usage_once


class InvertedResidualDown(nn.Module):
    def __init__(
            self, inp: int, oup: int, stride: int, expand_ratio: int, norm_layer: Optional[Callable[..., nn.Module]] = None
    ) -> None:
        super().__init__()
        self.stride = stride
        assert stride in [1, 2]

        if norm_layer is None:
            norm_layer = nn.BatchNorm2d

        hidden_dim = int(round(inp * expand_ratio))
        self.use_res_connect = self.stride == 1 and inp == oup

        layers: List[nn.Module] = []
        if expand_ratio != 1:
            # pw
            layers.append(
                ConvNormActivation(inp, hidden_dim, kernel_size=1, norm_layer=norm_layer, activation_layer=nn.ReLU6)
            )
        layers.extend(
            [
                # dw
                ConvNormActivation(
                    hidden_dim,
                    hidden_dim,
                    stride=stride,
                    groups=hidden_dim,
                    norm_layer=norm_layer,
                    activation_layer=nn.ReLU6,
                ),
                # pw-linear
                nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                norm_layer(oup),
            ]
        )
        self.conv = nn.Sequential(*layers)
        self.out_channels = oup
        self._is_cn = stride > 1

    def forward(self, x: Tensor) -> Tensor:
        if self.use_res_connect:
            return x + self.conv(x)
        else:
            return self.conv(x)


class InvertedResidualUp(nn.Module):
    def __init__(
            self, inp: int, oup: int, stride: int, expand_ratio: int, norm_layer: Optional[Callable[..., nn.Module]] = None
    ) -> None:
        super().__init__()
        self.stride = stride
        assert stride in [1, 2]

        if norm_layer is None:
            norm_layer = nn.BatchNorm2d

        hidden_dim = int(round(inp * expand_ratio))
        self.use_res_connect = self.stride == 1 and inp == oup

        layers: List[nn.Module] = []
        if stride == 1:
            if expand_ratio != 1:
                # pw
                layers.append(
                    ConvNormActivation(inp, hidden_dim, kernel_size=1, norm_layer=norm_layer, activation_layer=nn.ReLU6)
                )

            layers.extend(
                [
                    # dw
                    ConvNormActivation(
                        hidden_dim,
                        hidden_dim,
                        stride=stride,
                        groups=hidden_dim,
                        norm_layer=norm_layer,
                        activation_layer=nn.ReLU6,
                    ),

                    # pw-linear
                    nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                    norm_layer(oup),
                ]
            )
        else:
            depthwise_deconv = True
            if depthwise_deconv:
                if expand_ratio != 1:
                    # pw
                    layers.append(
                        ConvNormActivation(inp, hidden_dim, kernel_size=1, norm_layer=norm_layer, activation_layer=nn.ReLU6)
                    )
                layers.extend(
                    [
                        # dw
                        nn.ConvTranspose2d(hidden_dim, hidden_dim, groups=hidden_dim, kernel_size=4, stride=2, padding=1, bias=False),
                        # norm_layer(hidden_dim),
                        # nn.ReLU(),

                        # pw-linear
                        nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                        norm_layer(oup),
                    ]
                )
            else:
                # normal deconvolution
                layers.extend(
                    [
                        nn.ConvTranspose2d(inp, oup, kernel_size=2, stride=2, padding=0, bias=False),
                        # norm_layer(oup),
                        # nn.ReLU(),
                    ]
                )
        self.conv = nn.Sequential(*layers)
        self.out_channels = oup
        self._is_cn = stride > 1

    def forward(self, x: Tensor) -> Tensor:
        if self.use_res_connect:
            return x + self.conv(x)
        else:
            return self.conv(x)


class MobileNetV2Encoder(nn.Module):
    def __init__(
            self,
            block: Optional[Callable[..., nn.Module]] = None,
            width_mult: float = 1.0,
            inverted_residual_setting: Optional[List[List[int]]] = None,
            round_nearest: int = 8,
            norm_layer: Optional[Callable[..., nn.Module]] = None,
            num_input_channels=3
    ) -> None:
        super().__init__()
        _log_api_usage_once(self)

        if block is None:
            block = InvertedResidualDown

        if norm_layer is None:
            norm_layer = nn.BatchNorm2d

        d = 1

        input_channel = 16*d


        if inverted_residual_setting is None:
            inverted_residual_setting = [
                # t, c, n, s
                # [1, 16, 1, 2],
                # [6, 24, 2, 2],
                # [6, 32, 3, 2],
                # [6, 64, 4, 2],
                [1, 16*d, 1, 2],
                [6, 24*d, 2, 2],
                [6, 32*d, 3, 2],

                # [1, 16 * 1, 1, 2],
                # [1, 64, 2, 2],
                # [1, 128, 3, 2],

                [1, 256, 1, 1],

                # [6, 96, 3, 1],
                # [6, 160, 3, 2],
                # [6, 320, 1, 1],
            ]

        # only check the first element, assuming user knows t,c,n,s are required
        if len(inverted_residual_setting) == 0 or len(inverted_residual_setting[0]) != 4:
            raise ValueError(
                f"inverted_residual_setting should be non-empty or a 4-element list, got {inverted_residual_setting}"
            )

        # building first layer
        input_channel = _make_divisible(input_channel * width_mult, round_nearest)
        features: List[nn.Module] = [
            ConvNormActivation(num_input_channels, input_channel, stride=2, norm_layer=norm_layer, activation_layer=nn.ReLU6)
        ]

        # building inverted residual blocks
        for t, c, n, s in inverted_residual_setting:
            output_channel = _make_divisible(c * width_mult, round_nearest)
            for i in range(n):
                stride = s if i == 0 else 1
                features.append(block(input_channel, output_channel, stride, expand_ratio=t,
                                      norm_layer=norm_layer))
                input_channel = output_channel

        # make it nn.Sequential
        self.features = nn.Sequential(*features)

        # weight initialization
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode="fan_out")
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.zeros_(m.bias)

    def _forward_impl(self, x: Tensor) -> Tensor:
        # This exists since TorchScript doesn't support inheritance, so the superclass method
        # (this one) needs to have a name other than `forward` that can be accessed in a subclass
        x = self.features(x)

        # Cannot use "squeeze" as batch-size can be 1
        # x = nn.functional.adaptive_avg_pool2d(x, (1, 1))
        # x = torch.flatten(x, 1)

        return x

    def forward(self, x: Tensor) -> Tensor:
        return self._forward_impl(x)


class MobileNetV2Decoder(nn.Module):
    def __init__(
            self,
            num_output_channels,
            block: Optional[Callable[..., nn.Module]] = None,
            width_mult: float = 1.0,
            inverted_residual_setting: Optional[List[List[int]]] = None,
            round_nearest: int = 8,
            norm_layer: Optional[Callable[..., nn.Module]] = None,
    ) -> None:
        super().__init__()
        _log_api_usage_once(self)

        if block is None:
            block = InvertedResidualUp

        if norm_layer is None:
            norm_layer = nn.BatchNorm2d

        # input_channel = 64
        input_channel = 256

        if inverted_residual_setting is None:
            inverted_residual_setting = [
                # t, c, n, s
                # [6, 64, 4, 2],
                [6, 32, 2, 1],  # 14 -> 28
                # [6, 256, 3, 2],  # 14 -> 28
                [6, 24, 2, 2],  # 28 -> 56
                [1, 16, 1, 2],  # 56 -> 112
            ]

        # only check the first element, assuming user knows t,c,n,s are required
        if len(inverted_residual_setting) == 0 or len(inverted_residual_setting[0]) != 4:
            raise ValueError(
                f"inverted_residual_setting should be non-empty or a 4-element list, got {inverted_residual_setting}"
            )

        features: List[nn.Module] = []

        # building inverted residual blocks
        for t, c, n, s in inverted_residual_setting:
            output_channel = _make_divisible(c * width_mult, round_nearest)
            for i in range(n):
                stride = s if i == 0 else 1
                features.append(block(input_channel, output_channel, stride, expand_ratio=t,
                                      norm_layer=norm_layer))
                input_channel = output_channel

        self.num_output_channels = num_output_channels

        # make it nn.Sequential
        self.features = nn.Sequential(*features)

        # # building last several layers
        self.out: List[nn.Module] = []
        self.out.extend(
            [
                nn.ConvTranspose2d(input_channel, input_channel, kernel_size=4, stride=2, padding=1, bias=False),
                nn.ConvTranspose2d(input_channel, self.num_output_channels, kernel_size=4, stride=2, padding=1, bias=False)
            ]
        )
        self.out = nn.Sequential(*self.out)

        # weight initialization
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode="fan_out")
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.zeros_(m.bias)

    def _forward_impl(self, x: Tensor) -> Tensor:
        # This exists since TorchScript doesn't support inheritance, so the superclass method
        # (this one) needs to have a name other than `forward` that can be accessed in a subclass
        for ft in self.features:
            x = ft(x)

        x = self.out(x)

        return x

    def forward(self, x: Tensor) -> Tensor:
        return self._forward_impl(x)

